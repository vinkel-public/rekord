/* eslint no-process-env: 0 */

module.exports = {
  devServer: {
    host: "0.0.0.0",
    port: 8011,
    public: process.env.HMR_CLIENT_PUBLIC,
    disableHostCheck: true,
  },
  transpileDependencies: ["vuetify"],
};

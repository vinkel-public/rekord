# rekord

## Running webpack with correct binding for WHR

  ./node_modules/.bin/vue-cli-service serve --public localhost:8011

Using the --public option for the dev server will set the WHR bind.
Useful if the auto-detection does not use 'localhost'.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

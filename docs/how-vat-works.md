# VAT support in Rekord

Rekord has a basic implementation of VAT to support the norwegian market.

* VAT is enabled/disabled per Organization
* The four common norwegian VAT classes is pre-defined and used across all organizations
	* 0% - VAT exempt products
	* 12% - Transportation, tickets, etc
	* 15% - Food related items
	* 25% - General high VAT class used for most products
* Each order has a vat_enabled field for whether VAT was activated in organization at order time
* Each order has a copy of the VAT class percentage for the delivery method chosen at order time
* Each orderline has a copy of the VAT class percentage for the product at order time
* All prices specified on products is the full price including VAT


## Workflow for new customers to calculate VAT

### For customers already registered to collect VAT (Merverdiavgiftsregisteret)

 1. Set VAT to Enabled in the organization settings
 2. Set appropriate VAT classes in products and delivery methods configuration
 3. VAT will now be calculated for all orders

### For customers not yet registered to collect VAT (Merverdiavgiftsregisteret)

 1. Set VAT to Disabled in the organization settings
 2. Set appropriate VAT classes in products and delivery methods configuration
 3. VAT will not be calculated on orders since it is disabled
 4. Wait until you are registered to collect VAT
 5. Go to organization settings and enable VAT
 6. VAT will now be collected on all orders since VAT was enabled

## Rounding concerns

The current implementation is a bit naive and might introduce a rounding error unless tested in more detail.

* Server side calculation of VAT uses Python's Decimal default settings which is ROUND_HALF_EVEN
* Client side calculation of VAT uses floating point numbers with "toFixed" to do rounding
*  We might consider using a library like currency.js or dinero.js to assist us with both rounding as well as currency formatting

We have done some basic testing and not been able to spot any incorrect rounding. The impact will at most be 1 øre (0.01 NOK) which we can accept right now to get to MVP.

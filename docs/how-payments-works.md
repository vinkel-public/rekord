
# Payments in Rekord

In order to have a generic approach to payments we have decided to use the [django-payments](https://django-payments.readthedocs.io/) package. This lets us have a generic "Payment" model regardless of what payment method (called "providers" in django-payments) is being used and offer a unified way to deal with payment statuses and actions like capture, refund and release.

## Dealing with `provider.get_form()` 

Each provider in django-payments is expected to implement a `get_form()` method which does a lot of things. It is responsible for:

* Returning a Django form to ask for payment related information
* Process a POST for the mentioned form and proceed accordingly
* Create payments externally using API if necessary
* Store payment related information in a `extra_data` blob field (eg; API response objects)
* Redirect client to success pages and/or 3rd party URLs for further processing
* Manage payment model statuses and changes based on input from form, results from API and more

Dealing with all these different concepts in a single get_form method can be a bit confusing. And some more problems occur when our frontend is a Vue application that doesn't know how to deal with django forms as such.

**The Rekord application currently does not use or support any django-payment providers that implements `get_form()` as actually returning a Django form object. Neither Vipps, Stripe nor Offline needs any form as such and thus this problem is avoided for now.**

## REST API for managing payments
As our frontend is a Vue application communicating with our REST API we need to break down the different parts of the Payment lifecycle in django-payments as available endpoints in our API.

The current implementation is as follows:
* Create a new payment by sending a POST to /payments/
	* Returns payment object with ID if successful
* When ready to process payment send a POST to /payments/&lt;payment_id&gt;/process/
	* Returns object with a `success` field to indicate success
	* If successful, object can have a `redirect` field (used for 3rd party redirect)
* The `process` endpoint can be used later to check for updated status on a payment
	* For example when coming back from an external payment gateway

If we combine the process laid out above with our frontend implementation it would be as follows:
* Customer fills out his details and clicks "Place order" in Checkout.vue
* Checkout.vue uses API to generate order, orderlines and payment objects
* Checkout.vue redirects customer to PaymentLoading.vue for processing payment
* PaymentLoading.vue POSTs to "process" endpoint to initiate payment processing
	* If process endpoint returns success=True but no redirect URL, redirect to VendorOrderPlaced.vue
	* If process endpoint returns success=True with a redirect URL, redirect to the redirect URL
	* If process endpoint returns success=False, redirect to VendorOrderFailed.vue
* If the provider requires redirecting to a 3rd party like Vipps:
	* The "return URL" for the payment is served by Django and redirects back to the PaymentLoading.vue frontend component
	* PaymentLoading.vue will again POST to "process" to check if status has been updated, and redirect accordingly to success/failure views

## Supported django-payments providers

### Vipps
We have built a custom Vipps provider which integrates with the Vipps REST API. `get_form()` will change status from `PaymentStatus.WAITING` to `PaymentStatus.INPUT` and try to generate a payment using their API. If successful it will raise `RedirectNeeded` to the external Vipps URL where the customer completes the payment. 

The customer is redirected back to our application after successful or failed payment. Vipps will however send a callback request to our Django application with updated information about the payment. When the customer is redirected back this callback might not have been called yet, so status might still be unknown. For this scenario we need  to check payment status directly using the Vipps API (currently not implemented).

### Stripe
The supplied Stripe provider from django-payments has some flaws and is currently not used until we have replaced it with something else.

The main problem with it is that it utilizes a modal payment popup to complete the payment which comes directly from Stripes Checkout.js library. This is not consistent with our Vuetify application experience. It also uses `get_form()` in a strange way, as you are supposed to render it as a form, but in practice only renders a Stripe-branded button "Pay with Stripe".

### Offline
The poorly named "Offline" provider is very simple and does not speak to any endpoints. New payments will get status PaymentStatus.PREAUTH when get_form is provided and redirect to the success page. Using `PaymentStatus.PREAUTH` allows for regular "Capture", "Refund" and "Cancel" events to indicate Paid, Refunded and Cancelled states.

## Problem with `settings.PAYMENT_VARIANTS`

django-payment expects the configured payment methods ("variants") to be part of Django's settings module. This doesn't match our usage as we would like the configuration to come from each individual Organization model with their own keys.

The current solution which has some obvious flaws is to manually set `settings.PAYMENT_VARIANTS` on the fly before using the provider_factory utility method from django-payments to get an instance of a configured provider. This sort of works, but might have some unintended consequences when deployed in different environments. Changing the Django settings module directly on the fly is generally not recommended.

### Possible solutions

* Fork django-payments and make adjustments to for example allow the PAYMENT_VARIANTS setting to come from the Payment model instead. This might even be accepted back into django-payments in the form of a pull request

* Wrap changing the settings dynamically in a context manager which always will reset the settings after each use, might help avoid unintended consequences

## TODO
* Implement a variant of the original django-payments Stripe provider that behaves like we want
* Coming back from Vipps will only work if the callback behind the scenes already have successfully happened
* Write tests for Vipps provider implementation
* Discuss and choose a path moving forward with `settings.PAYMENT_VARIANTS` challenge

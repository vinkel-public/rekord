import {
  Organizations,
  Vendors,
  Products,
  Orders,
  Orderlines,
  Payments,
  VippsConfigurations,
  StripeConfigurations,
  OfflineConfigurations,
  Marketplaces,
} from "@/local-models";

const resources = {};

resources["organizations"] = new Organizations();

const organization1 = resources["organizations"].add({
  name: "Vinkel org",
});

resources["vippsconfigurations"] = new VippsConfigurations();
resources["stripeconfigurations"] = new StripeConfigurations();
resources["offlineconfigurations"] = new OfflineConfigurations();

var offlineconfig1 = resources["offlineconfigurations"].add({
  organization: organization1.id,
  name: "My offline config",
  frontend_label: "Cash on delivery",
  description: "Keep your cash handy",
});

resources["vendors"] = new Vendors();

const vendor1 = resources["vendors"].add({
  organization: organization1.id,
  name: "Vinkel shop",
  stripe_enabled: false,
  vipps_enabled: false,
  offline_enabled: true,
  featured: true,
  enabled_payment_methods: [
    {
      frontend_label: offlineconfig1.frontend_label,
      description: offlineconfig1.description,
      variant: "offline",
    },
  ],
  configured_payment_methods: ["offline"],
});

const vendor2 = resources["vendors"].add({
  organization: organization1.id,
  name: "Realest farm in town",
  stripe_enabled: false,
  vipps_enabled: false,
  offline_enabled: true,
  featured: true,
  enabled_payment_methods: [
    {
      frontend_label: offlineconfig1.frontend_label,
      description: offlineconfig1.description,
      variant: "offline",
    },
  ],
  configured_payment_methods: ["offline"],
});

resources["products"] = new Products();

const product1 = resources["products"].add({
  vendor: vendor1.id,
  name: "Shop product 1",
  description: "Shop product is for testing purposes only",
  price: 100,
});

const product2 = resources["products"].add({
  vendor: vendor1.id,
  name: "Shop product 2",
  description: "Shop product is for testing purposes only",
  price: 50,
});

resources["products"].add({
  vendor: vendor2.id,
  name: "Farm product 1",
  description: "Real product is for testing purposes only",
  price: 100,
});

resources["products"].add({
  vendor: vendor2.id,
  name: "Farm product 2",
  description: "Real product is for testing purposes only",
  price: 50,
});

resources["orders"] = new Orders();

const order1 = resources["orders"].add({
  vendor: vendor1.id,
  name: "Ola Nordmann",
  phone: "42123242",
  email: "ola@nordmann.com",
  payment: "vipps",
  payment_status: "confirmed",
  delivered: false,
  total: 123,
});

const order2 = resources["orders"].add({
  vendor: vendor1.id,
  name: "Kari Nordmann",
  phone: "12345678",
  email: "kari@nordmann.com",
  payment: "vipps",
  payment_status: "preauth",
  delivered: true,
  total: 123,
});

resources["orderlines"] = new Orderlines();

resources["orderlines"].add({
  order: order1.id,
  product: product1.id,
  quantity: 1,
});

resources["orderlines"].add({
  order: order1.id,
  product: product2.id,
  quantity: 2,
});

resources["orderlines"].add({
  order: order2.id,
  product: product1.id,
  quantity: 3,
});

resources["orderlines"].add({
  order: order2.id,
  product: product2.id,
  quantity: 1,
});

resources["payments"] = new Payments();

resources["payments"].add({
  order: order1.id,
  total: order1.total,
  variant: order1.payment,
  status: order1.payment_status,
  transaction_id: 123456,
  token: "4c7e9bd6-bb7e-4644-a2a9-1ea7749f01ec",
});

resources["payments"].add({
  order: order2.id,
  total: order2.total,
  variant: order2.payment,
  status: order2.payment_status,
  transaction_id: 123457,
  token: "11fcef81-9d42-44d7-8c49-e76555e658a4",
});

resources["marketplaces"] = new Marketplaces();

resources["marketplaces"].add({
  name: "Tinga markedsplass",
  description: "Tinga er en markedsplass.",
  image: null,
  vendors: [vendor1, vendor2],
});

export default resources;

import App from "@/App.vue";
import store from "@/store";
import router from "@/router";
import Vue from "vue";
import vuetify from "@/plugins/vuetify";

Vue.use(vuetify);

Vue.config.productionTip = false;

const app = new Vue({
  render: (h) => h(App),
  router: router,
  vuetify: vuetify,
  store: store,
}).$mount("#app");

if (process.env.NODE_ENV == "development") {
  // Export root Vue object if we are in dev mode
  window.app = app;
}

const baseUrlDefault = "https://api.rekord.app/";
const baseUrl = process.env.VUE_APP_REKORDKEEPER_BASE_URL
  ? process.env.VUE_APP_REKORDKEEPER_BASE_URL
  : baseUrlDefault;

let authState = {};

const getFetchOptions = function (method, data, options = {}) {
  const isFormData = data instanceof FormData;

  let headers = {};

  if (!isFormData) {
    headers["Content-Type"] = "application/json";
  }

  let token = undefined;

  if (options.token) {
    token = options.token;
  } else if (authState.token) {
    token = authState.token;
  }

  if (token) {
    headers["Authorization"] = "Token " + token;
  }

  if (!isFormData) {
    data = JSON.stringify(data);
  }

  return {
    body: data,
    headers: headers,
    method: method,
  };
};

const fetchAndCheck = async function (resource, init, options = {}) {
  const response = await fetch(resource, init);

  if (!response.ok && !options.passResponse) {
    console.error("Request failed on resource", resource);
    throw new Error("Request failed");
  }

  if (options.passResponse) {
    return response;
  }

  return response.json();
};

const urlObject = function (_url, searchParams) {
  const url = new URL(baseUrl + _url);

  for (const [k, v] of Object.entries(searchParams)) {
    url.searchParams.append(k, v);
  }

  return url;
};

export const get = async function (_url, searchParams = {}, options = {}) {
  const url = urlObject(_url, searchParams);

  return await fetchAndCheck(
    url,
    getFetchOptions("GET", undefined, options),
    options
  );
};

export const post = async function (
  _url,
  data,
  searchParams = {},
  options = {}
) {
  const url = urlObject(_url, searchParams);

  return await fetchAndCheck(
    url,
    getFetchOptions("POST", data, options),
    options
  );
};

export const put = async function (
  _url,
  data,
  searchParams = {},
  options = {}
) {
  const url = urlObject(_url, searchParams);

  return await fetchAndCheck(
    url,
    getFetchOptions("PUT", data, options),
    options
  );
};

export const del = async function (
  _url,
  data,
  searchParams = {},
  options = {}
) {
  const url = urlObject(_url, searchParams);

  return await fetchAndCheck(
    url,
    getFetchOptions("DELETE", data, options),
    options
  );
};

export const setAuthState = function (newState) {
  authState = newState;
};

import generatedData from "@/generate-data";
const resources = generatedData;

export const get = async function (_url, params = {}) {
  const [resourceName, id] = _url.split("/");
  let data = resources[resourceName];

  // Filter resources using provided params if any
  for (const [key, value] of Object.entries(params)) {
    data = data.filter((el) => el[key] == value);
  }

  // If we don't have an ID, return the list
  if (id == "") {
    const res = JSON.parse(JSON.stringify(data));
    return res;
  }

  // Return the object referenced by the ID
  const res = JSON.parse(JSON.stringify(data.find((el) => el.id == id)));
  return res;
};

export const put = async function (_url, data) {
  const [resourceName, id, action] = _url.split("/");

  let resourceIndex = undefined;
  let resource = undefined;

  data = JSON.parse(JSON.stringify(data));

  resources[resourceName].forEach((el, idx) => {
    if (el.id == id) {
      resourceIndex = idx;
      resource = el;
    }
  });

  if (typeof action !== "undefined" && action != "") {
    // If we have an action, run a method on our model instance
    // instead of replacing field values
    resource[action](data);
  } else {
    // Only update fields that are on the resource itself,
    // skip provided fields that are not already on the resource
    for (const [key, value] of Object.entries(data)) {
      if (typeof resource[key] !== "undefined") {
        resource[key] = value;
      }
    }
  }

  resources[resourceName][resourceIndex] = resource;

  return resource;
};

export const post = async function (_url, data) {
  const [resourceName, id, action] = _url.split("/");

  let resource = undefined;

  data = JSON.parse(JSON.stringify(data));

  if (typeof id !== "undefined") {
    resources[resourceName].forEach((el) => {
      if (el.id == id) {
        resource = el;
      }
    });
  }

  if (typeof resource == "undefined") {
    resources[resourceName].add(data);
  } else {
    // If we have an action, run a method on our model instance
    // instead of replacing field values
    data = resource[action](data);
  }

  return data;
};

export const del = async function (_url) {
  const [resourceName, id] = _url.split("/");

  let resourceIndex = undefined;

  resources[resourceName].forEach((el, idx) => {
    if (el.id == id) {
      resourceIndex = idx;
    }
  });

  resources[resourceName].splice(resourceIndex, 1);
  return {};
};

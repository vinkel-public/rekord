import * as fetchBackend from "@/fetch";
import * as localBackend from "@/local-backend";

let exportBackend = fetchBackend;

if (process.env.VUE_APP_BACKEND == "local") {
  exportBackend = localBackend;
}

export default exportBackend;

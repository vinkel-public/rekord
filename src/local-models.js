class Model {
  constructor(data) {
    for (const [propName, value] of Object.entries(data)) {
      this[propName] = value;
    }

    for (const [fieldName, cb] of Object.entries(this.getFields())) {
      this[fieldName] = cb();
    }
  }

  getFields() {
    return {};
  }
}

export class Models extends Array {
  constructor() {
    super();
    this._idCounter = 0;
  }
  getModelKlass() {
    return Model;
  }

  add(data) {
    const modelKlass = this.getModelKlass();
    data.id = ++this._idCounter;
    this.push(new modelKlass(data));

    return data;
  }
}

export class Organization extends Model {}

export class Organizations extends Models {
  getModelKlass() {
    return Organization;
  }
}

export class Vendor extends Model {}

export class Vendors extends Models {
  getModelKlass() {
    return Vendor;
  }
}

export class Product extends Model {}

export class Products extends Models {
  getModelKlass() {
    return Product;
  }
}

export class Order extends Model {
  getFields() {
    return {
      paid: () => false,
      ordered_at: () => new Date(),
      payment_status: () => "preauth",
      delivered: () => false,
    };
  }
}

export class Orders extends Models {
  getModelKlass() {
    return Order;
  }
}

export class Orderline extends Model {}

export class Orderlines extends Models {
  getModelKlass() {
    return Orderline;
  }
}

export class Payment extends Model {
  capture() {
    this.status = "confirmed";
  }
  release() {
    this.status = "refunded";
  }
  refund() {
    this.status = "refunded";
  }
  async process() {
    this.status = "preauth";

    function wait(ms) {
      return new Promise((resolve) => setTimeout(resolve, ms));
    }

    // Let the spinning progress bar actually appear a little while
    await wait(2000);

    return {
      success: true,
    };
  }
}

export class Payments extends Models {
  getModelKlass() {
    return Payment;
  }
}

export class VippsConfiguration extends Model {}

export class VippsConfigurations extends Models {
  getModelKlass() {
    return VippsConfiguration;
  }
}

export class StripeConfiguration extends Model {}

export class StripeConfigurations extends Models {
  getModelKlass() {
    return StripeConfiguration;
  }
}

export class OfflineConfiguration extends Model {}

export class OfflineConfigurations extends Models {
  getModelKlass() {
    return OfflineConfiguration;
  }
}

export class Marketplace extends Model {}

export class Marketplaces extends Models {
  getModelKlass() {
    return Marketplace;
  }
}

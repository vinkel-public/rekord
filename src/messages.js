const state = {
  messages: [],
};

const mutations = {
  add(state, payload) {
    state.messages.push(payload);
  },

  /*
  remove(state, payload) {
    state.messages = state.messages.filter(msg => {
      if (msg == payload) {
        return false;
      }
      return true;
    });
  },
  */
};

const actions = {
  add({ commit }, payload) {
    commit("add", payload);
  },

  /*
  remove({ commit }, payload) {
    commit("remove", payload);
  },
  */
};

export default {
  state,
  actions,
  mutations,
};

import backend from "@/backend";

const LOCALSTORAGE_KEY = "rekord_auth";

const initialState = {
  name: "<anonymous user>",
  anonymous: true,
  organization: {},
};

let state = { ...initialState };

if (LOCALSTORAGE_KEY in localStorage) {
  state = JSON.parse(localStorage[LOCALSTORAGE_KEY]);

  if (state.token) {
    backend.setAuthState({
      token: state.token,
    });
  }
}

const mutations = {
  login(state, payload) {
    state.token = payload.token;
    state.name = payload.name;
    state.anonymous = payload.anonymous;
    state.organization.id = payload.organization.id;
    state.organization.name = payload.organization.name;

    localStorage[LOCALSTORAGE_KEY] = JSON.stringify(state);
  },

  logout(state) {
    state.name = initialState.name;
    state.anonymous = initialState.anonymous;

    localStorage.removeItem(LOCALSTORAGE_KEY);
  },
};

const actions = {
  async login({ commit }, payload) {
    const response = await backend.post(
      "users/login/",
      {
        username: payload.username,
        password: payload.password,
      },
      {},
      {
        passResponse: true,
      }
    );

    if (response.ok) {
      const responseData = await response.json();

      backend.setAuthState({
        token: responseData.token,
      });

      commit("login", {
        ...responseData,
      });
    }
  },

  async logout({ commit }) {
    backend.setAuthState({});
    commit("logout");
  },
};

export default {
  state,
  actions,
  mutations,
};

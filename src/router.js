import AdminPaymentConfiguration from "@/components/AdminPaymentConfiguration";
import AdminDeliveryMethods from "@/components/AdminDeliveryMethods";
import AdminOrderDetail from "@/components/AdminOrderDetail";
import AdminOrders from "@/components/AdminOrders";
import AdminProductDetail from "@/components/AdminProductDetail";
import AdminProducts from "@/components/AdminProducts";
import AdminProductCreate from "@/components/AdminProductCreate";
import AdminVendors from "@/components/AdminVendors";
import AdminVendorPayment from "@/components/AdminVendorPayment";
import AdminVendorShipping from "@/components/AdminVendorShipping";
import AdminVendorDetail from "@/components/AdminVendorDetail";
import AdminVendorEdit from "@/components/AdminVendorEdit";
import AdminOrganizationSettings from "@/components/AdminOrganizationSettings";
import PaymentLoading from "@/components/PaymentLoading";
import Home from "@/components/Home";
import MarketplaceDetail from "@/components/MarketplaceDetail";
import Admin from "@/components/Admin.vue";
import Login from "@/components/Login.vue";
import Logout from "@/components/Logout.vue";
import NotFound from "@/components/NotFound";
import Vendor from "@/components/Vendor";
import VendorTerms from "@/components/VendorTerms";
import VendorProductDetail from "@/components/VendorProductDetail";
import VendorOrderPlaced from "@/components/VendorOrderPlaced";
import VendorOrderFailed from "@/components/VendorOrderFailed";
import Vue from "vue";
import VueRouter from "vue-router";
import store from "@/store";

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    {
      component: Home,
      name: "home",
      path: "/",
      meta: {
        requiresAdmin: false,
      },
    },

    {
      component: Login,
      name: "login",
      path: "/login",
      meta: {
        requiresAdmin: false,
      },
    },

    {
      component: Logout,
      name: "logout",
      path: "/logout",
      meta: {
        requiresAdmin: false,
      },
    },

    {
      component: Admin,
      name: "admin",
      path: "/admin",
      meta: {
        requiresAdmin: true,
      },
    },

    {
      component: AdminOrganizationSettings,
      name: "adminOrganizationSettings",
      path: "/admin/organization/settings",
      props: true,
      meta: {
        requiresAdmin: true,
      },
    },

    {
      component: AdminPaymentConfiguration,
      name: "adminPaymentConfiguration",
      path: "/admin/organization/payment-config",
      props: true,
      meta: {
        requiresAdmin: true,
      },
    },

    {
      component: AdminDeliveryMethods,
      name: "adminDeliveryMethods",
      path: "/admin/organization/delivery-methods",
      props: true,
      meta: {
        requiresAdmin: true,
      },
    },

    {
      component: MarketplaceDetail,
      name: "marketplace",
      path: "/marketplace/:marketplaceId",
      props: true,
      meta: {
        requiresAdmin: false,
      },
    },

    {
      component: Vendor,
      name: "vendor",
      path: "/vendor/:vendorId",
      props: true,
      meta: {
        requiresAdmin: false,
      },
    },

    {
      component: VendorProductDetail,
      name: "vendorProductDetail",
      path: "/vendor/:vendorId/product/:productId",
      props: true,
      meta: {
        requiresAdmin: false,
      },
    },

    {
      component: PaymentLoading,
      name: "paymentLoading",
      path: "/payment/:paymentId",
      props: true,
      meta: {
        requiresAdmin: false,
      },
    },

    {
      component: Vendor,
      name: "vendorWithEvent",
      path: "/vendor/:vendorId/events/:eventId",
      props: true,
      meta: {
        requiresAdmin: false,
      },
    },

    {
      component: VendorTerms,
      name: "vendorTerms",
      path: "/vendor/:vendorId/terms",
      props: true,
      meta: {
        requiresAdmin: false,
      },
    },

    {
      component: AdminOrders,
      name: "adminOrders",
      path: "/vendor/:vendorId/admin",
      props: true,
      meta: {
        requiresAdmin: true,
      },
    },

    {
      component: VendorOrderPlaced,
      name: "vendor-order-placed",
      path: "/vendor/:vendorId/success/:orderId",
      props: true,
      meta: {
        requiresAdmin: false,
      },
    },

    {
      component: VendorOrderFailed,
      name: "vendor-order-failed",
      path: "/vendor/:vendorId/failed/:orderId",
      props: true,
      meta: {
        requiresAdmin: false,
      },
    },

    {
      component: AdminOrderDetail,
      name: "adminOrderDetail",
      path: "/vendor/:vendorId/admin/orders/:orderId",
      props: true,
      meta: {
        requiresAdmin: true,
      },
    },

    {
      component: AdminProducts,
      name: "adminProducts",
      path: "/vendor/:vendorId/admin/products",
      props: true,
      meta: {
        requiresAdmin: true,
      },
    },

    {
      component: AdminProductCreate,
      name: "adminProductCreate",
      path: "/vendor/:vendorId/admin/products/create",
      props: true,
      meta: {
        requiresAdmin: true,
      },
    },

    {
      component: AdminProductDetail,
      name: "adminProductDetail",
      path: "/vendor/:vendorId/admin/products/:productId",
      props: true,
      meta: {
        requiresAdmin: true,
      },
    },

    {
      component: AdminVendors,
      name: "adminVendors",
      path: "/admin/vendors",
      props: true,
      meta: {
        requiresAdmin: true,
      },
    },

    {
      component: AdminVendorShipping,
      name: "adminVendorShipping",
      path: "/admin/vendors/:vendorId/shipping",
      props: true,
      meta: {
        requiresAdmin: true,
      },
    },

    {
      component: AdminVendorPayment,
      name: "adminVendorPayment",
      path: "/admin/vendors/:vendorId/payment",
      props: true,
      meta: {
        requiresAdmin: true,
      },
    },

    {
      component: AdminVendorEdit,
      name: "adminVendorCreate",
      path: "/admin/vendors/create",
      meta: {
        requiresAdmin: true,
      },
    },

    {
      component: AdminVendorEdit,
      name: "adminVendorEdit",
      path: "/admin/vendors/:vendorId/edit",
      props: true,
      meta: {
        requiresAdmin: true,
      },
    },

    {
      component: AdminVendorDetail,
      name: "adminVendorDetail",
      path: "/admin/vendors/:vendorId",
      props: true,
      meta: {
        requiresAdmin: true,
      },
    },

    {
      component: NotFound,
      name: "notFound",
      path: "*",
      meta: {
        requiresAdmin: false,
      },
    },
  ],
});

router.beforeEach((to, from, next) => {
  // We assume that all routes require auth, so we check for an
  // explicit false value on the flag
  if (to.meta.requiresAdmin === false || !store.state.auth.anonymous) {
    next();
    return;
  }

  next({
    name: "login",
  });
});

export default router;

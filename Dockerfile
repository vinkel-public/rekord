#syntax=docker/dockerfile:1.2
FROM node:14.16.0-buster as build-stage
WORKDIR /app
COPY package.json package-lock.json /app/
RUN --mount=type=cache,target=/root/.npm npm install
COPY . /app/
RUN npm run build

FROM nginx:1.18.0
COPY --from=build-stage /app/dist/ /usr/share/nginx/html

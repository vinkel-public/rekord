// Use eslint settings from package.json,
// but disable no-debugger rule if we are in development mode
let eslintConfig = require("./package.json").eslintConfig;

if (process.env.USE_PRETTIER) {
  eslintConfig.extends = [
    "plugin:vue/recommended",
    "plugin:prettier-vue/recommended",
    "prettier/vue",
  ];
}

if (process.env.NODE_ENV === "development") {
  eslintConfig.rules["no-debugger"] = "off";
}

module.exports = eslintConfig;

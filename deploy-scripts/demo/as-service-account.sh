#!/bin/bash

set -eu
set -o pipefail

# Remove previous frontend deployment and deploy fresh tarball
rm -rf /srv/sites/demo.rekord.app/public_html
mkdir /srv/sites/demo.rekord.app/public_html

# Extract frontend tarball to vhost public_html directory
tar xaf frontend.tar.bz2 -C /srv/sites/demo.rekord.app/public_html

# Remove previous backend deployment
rm -rf /srv/sites/api.demo.rekord.app/rekordkeeper
mkdir /srv/sites/api.demo.rekord.app/rekordkeeper
rm -rf /srv/sites/api.demo.rekord.app/public_html/static

# Remove and recreate venv
rm -rf /srv/sites/api.demo.rekord.app/venv
python3 -m venv /srv/sites/api.demo.rekord.app/venv

# Deploy fresh tarball
tar xaf backend.tar.gz -C /srv/sites/api.demo.rekord.app/rekordkeeper

# Activate venv and upgrade pip
. /srv/sites/api.demo.rekord.app/venv/bin/activate
pip install --upgrade pip

# Install project dependencies
pip install -r /srv/sites/api.demo.rekord.app/rekordkeeper/requirements.txt

# Run management command for creating database and data
(
	export REKORDKEEPER_STATIC_ROOT=/srv/sites/api.demo.rekord.app/public_html/static
	cd /srv/sites/api.demo.rekord.app/rekordkeeper/;
	make dummy_data;
	python manage.py create_dummy_payment_config /srv/sites/api.demo.rekord.app/secrets.json;
	python manage.py collectstatic --no-input;
)

# Write out systemd service configuration
cat << EOF > /srv/sites/api.demo.rekord.app/rekordkeeper/rekordkeeper-demo.service
[Unit]
Description = Rekordkeeper, backend for Rekord (demo)
After = network.target

[Service]
User = rekord
Group = nogroup
PrivateTmp = true
RuntimeDirectory=rekordkeeper-demo
PIDFile = /run/rekordkeeper-demo/rekordkeeper-demo.pid
WorkingDirectory = /srv/sites/api.demo.rekord.app/rekordkeeper
ExecStart = /srv/sites/api.demo.rekord.app/venv/bin/gunicorn rekordkeeper.wsgi --pid /run/rekordkeeper-demo/rekordkeeper-demo.pid  --error-logfile - --access-logfile -
Environment=REKORDKEEPER_PAYMENT_HOST=api.demo.rekord.app
Environment=REKORDKEEPER_STATIC_ROOT=/srv/sites/api.demo.rekord.app/public_html/static
Environment=REKORDKEEPER_DEFAULT_FROM_EMAIL=post+demo@rekord.app

[Install]
WantedBy = multi-user.target
EOF

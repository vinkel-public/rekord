#!/bin/bash

set -eu
set -o pipefail

# Remove previous frontend deployment and deploy fresh tarball
rm -rf /srv/sites/app.rekord.app/public_html
mkdir /srv/sites/app.rekord.app/public_html

# Extract frontend tarball to vhost public_html directory
tar xaf frontend.tar.bz2 -C /srv/sites/app.rekord.app/public_html

# Move previous backend deployment
BACKEND_BACKUP_PATH_POSTFIX=$(date --iso=ns)
if [ -d /srv/sites/api.rekord.app/rekordkeeper ]; then
	mv \
		/srv/sites/api.rekord.app/rekordkeeper \
		/srv/sites/api.rekord.app/rekordkeeper_"$BACKEND_BACKUP_PATH_POSTFIX"
fi

mkdir /srv/sites/api.rekord.app/rekordkeeper

# Remove previous collected static data
rm -rf /srv/sites/api.rekord.app/public_html/static

# Remove and recreate venv
rm -rf /srv/sites/api.rekord.app/venv
python3 -m venv /srv/sites/api.rekord.app/venv

# Deploy fresh tarball
tar xaf backend.tar.gz -C /srv/sites/api.rekord.app/rekordkeeper

# Copy in place previous deployment database and media
if [ -d /srv/sites/api.rekord.app/rekordkeeper_"$BACKEND_BACKUP_PATH_POSTFIX" ]; then
	cp -aR \
		/srv/sites/api.rekord.app/rekordkeeper_"$BACKEND_BACKUP_PATH_POSTFIX"/db.sqlite3 \
		/srv/sites/api.rekord.app/rekordkeeper_"$BACKEND_BACKUP_PATH_POSTFIX"/media \
		/srv/sites/api.rekord.app/rekordkeeper
fi

# Activate venv and upgrade pip
. /srv/sites/api.rekord.app/venv/bin/activate
pip install --upgrade pip

# Install project dependencies
pip install -r /srv/sites/api.rekord.app/rekordkeeper/requirements.txt

# Run management command for database migrations and static data collection
(
	cd /srv/sites/api.rekord.app/rekordkeeper/;
	python manage.py migrate --no-input;
	python manage.py collectstatic --no-input;
)

# Write out systemd service configuration
cat << EOF > /srv/sites/api.rekord.app/rekordkeeper/rekordkeeper.service
[Unit]
Description = Rekordkeeper, backend for Rekord
After = network.target

[Service]
User = rekord
Group = nogroup
PrivateTmp = true
RuntimeDirectory=rekordkeeper
PIDFile = /run/rekordkeeper/rekordkeeper.pid
WorkingDirectory = /srv/sites/api.rekord.app/rekordkeeper
ExecStart = /srv/sites/api.rekord.app/venv/bin/gunicorn rekordkeeper.wsgi -b 127.0.0.1:8001 --pid /run/rekordkeeper/rekordkeeper.pid --error-logfile - --access-logfile -

[Install]
WantedBy = multi-user.target
EOF

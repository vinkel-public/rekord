.PHONY: all
all:
	@echo Nothing to do for default target

.PHONY: clean
clean:
	rm -fr dist/
	rm -f frontend.tar.bz2
	rm -f backend.tar.gz

.PHONY: lint
lint:
	USE_PRETTIER=true npm run lint

.PHONY: test
test:
	$(MAKE) -C rekordkeeper docker-image docker-container

.PHONY: build
build: clean
	npm run build

.PHONY: tarballs
tarballs: build
	tar caf frontend.tar.bz2 -C dist .
	(cd rekordkeeper; git archive -o ../backend.tar.gz HEAD)

.PHONY: deploy_demo
deploy_demo:
	VUE_APP_DEMO_MODE=1 VUE_APP_REKORDKEEPER_BASE_URL="https://api.demo.rekord.app/" $(MAKE) tarballs
	scp -rp deploy-scripts frontend.tar.bz2 backend.tar.gz vinkel01:
	ssh vinkel01 ./deploy-scripts/demo/deploy.sh

.PHONY: deploy_production
deploy_production:
	$(MAKE) tarballs
	scp -rp deploy-scripts frontend.tar.bz2 backend.tar.gz vinkel01:
	ssh vinkel01 ./deploy-scripts/production/deploy.sh

.PHONY: docker-image
docker-image:
	docker image build -t rekord:latest .

.PHONY: docker-container
docker-container:
	docker container run --rm -it -p 127.0.0.1:8000:80 rekord:latest

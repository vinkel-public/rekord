from rest_framework import viewsets, serializers, routers
from rekordkeeper.permissions import ReadOnly

from rekordkeeper.vat.models import VatClass


class VatClassSerializer(serializers.ModelSerializer):
    class Meta:
        model = VatClass
        fields = [
            "id",
            "name",
            "percentage",
        ]


class VatClassViewSet(viewsets.ModelViewSet):
    """
        This resource should enforce permissions such that:
            * All users are allowed to read
            * Nobody is allowed to write
    """
    serializer_class = VatClassSerializer
    queryset = VatClass.objects.all()
    permission_classes = [ReadOnly]


router = routers.DefaultRouter()
router.register(r"vat", VatClassViewSet, basename="vat")

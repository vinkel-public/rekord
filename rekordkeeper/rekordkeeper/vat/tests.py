from django.test import TestCase

from decimal import Decimal

from rekordkeeper.orders.models import Order
from rekordkeeper.catalogues.models import Product
from rekordkeeper.vat.models import VatClass
from rekordkeeper.vendors.models import Vendor
from rekordkeeper.organizations.models import Organization
from rekordkeeper.shipping.models import DeliveryMethod


class VatTestCase(TestCase):
    def setUp(self):

        self.vat_classes = [
            VatClass.objects.create(name="Ikke momspliktig", percentage=0),
            VatClass.objects.create(name="Generell sats", percentage=25),
            VatClass.objects.create(name="Næringsmidler", percentage=15),
            VatClass.objects.create(
                name="Persontransport, kinobilletter, utleie av rom",
                percentage=12,
            ),
        ]

        self.organization = Organization.objects.create(
            name="Test organization",
        )

        self.delivery_method = DeliveryMethod.objects.create(
            name="Test delivery",
            price=0,
            organization=self.organization,
            vat_class=self.vat_classes[0],
        )

        self.vendor = Vendor.objects.create(
            organization=self.organization,
            name="Test vendor",
        )

    def create_products_with_vat_class(self, count, vat_class, price=100):
        products = []
        for x in range(count):
            products.append(
                Product.objects.create(
                    vendor=self.vendor,
                    name="Product {}".format(x),
                    price=price,
                    vat_class=vat_class,
                )
            )

        return products

    def test_get_vat_classes(self):
        response = self.client.get("/vat/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 4)

    def test_order_vat_free(self):
        vat_class = self.vat_classes[0]

        order = Order.objects.create(
            vendor=self.vendor,
            delivery_method=self.delivery_method,
        )

        for product in self.create_products_with_vat_class(1, vat_class):
            order.orderline_set.create(product=product, quantity=1)

        # Order total should be 100
        self.assertEqual(order.total, 100)

        # VAT total should be 0
        self.assertEqual(order.vat_total, 0)

        # Order excl VAT should be 100
        self.assertEqual(order.total_excluding_vat, 100)

    def test_order_vat_general(self):
        self.vendor.organization.vat_enabled = True

        vat_class = self.vat_classes[1]

        order = Order.objects.create(
            vendor=self.vendor,
            delivery_method=self.delivery_method,
        )

        for product in self.create_products_with_vat_class(1, vat_class):
            order.orderline_set.create(product=product, quantity=1)

        expected_vat = Decimal("20")

        # Order total should be 100
        self.assertEqual(order.total, 100)

        # VAT total should be 20
        self.assertEqual(order.vat_total, expected_vat)

        # Order excl VAT should be 80
        self.assertEqual(order.total_excluding_vat, Decimal("80"))

    def test_order_vat_food(self):
        self.vendor.organization.vat_enabled = True

        vat_class = self.vat_classes[2]

        order = Order.objects.create(
            vendor=self.vendor,
            delivery_method=self.delivery_method,
        )

        for product in self.create_products_with_vat_class(1, vat_class):
            order.orderline_set.create(product=product, quantity=1)

        expected_vat = Decimal("13.04")

        # Order total should be 100
        self.assertEqual(order.total, Decimal("100"))

        # VAT total should meet our expected VAT
        self.assertEqual(order.vat_total, expected_vat)

        # Order excl VAT should be 100 - expected_vat
        self.assertEqual(order.total_excluding_vat, 100 - expected_vat)

    def test_order_vat_food_org_vat_disabled(self):
        vat_class = self.vat_classes[2]

        order = Order.objects.create(
            vendor=self.vendor,
            delivery_method=self.delivery_method,
        )

        for product in self.create_products_with_vat_class(1, vat_class):
            order.orderline_set.create(product=product, quantity=1)

        # Order total should be 100
        self.assertEqual(order.total, Decimal("100"))

        # VAT total should be 0
        self.assertEqual(order.vat_total, 0)

        # Order excl VAT should be 100
        self.assertEqual(order.total_excluding_vat, Decimal("100"))

    def test_post_denied(self):
        response = self.client.post("/vat/", {
            "name": "Test VAT class",
            "percentage": 10,
        })
        self.assertEqual(response.status_code, 401)

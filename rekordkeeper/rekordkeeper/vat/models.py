from django.db import models


class VatClass(models.Model):
    name = models.CharField(max_length=100)
    percentage = models.PositiveSmallIntegerField()

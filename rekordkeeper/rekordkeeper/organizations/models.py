from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.utils.html import escape
from django.template.defaultfilters import linebreaksbr
from django.conf import settings

from rekordkeeper.numberseries.models import Numberseries


class OrganizationManager(models.Manager):
    def create(self, *kargs, **kwargs):
        # We are injecting some boilerplate terms
        # for each new organization
        with open(
            settings.BASE_DIR
            / "rekordkeeper"
            / "organizations"
            / "assets"
            / "terms.txt",
            "r",
        ) as fp:
            terms = fp.read()
            kwargs["terms"] = terms

        # Organizations need their own number series so lets spawn one
        # before creating the organization.
        number_series = Numberseries.objects.create(
            start_number=10001,
            current_number=10000,
            name="Order numberseries for {}".format(
                kwargs["name"])
        )

        kwargs["order_number_series"] = number_series
        return super().create(*kargs, **kwargs)


class Organization(models.Model):
    name = models.CharField(max_length=256)
    vat_enabled = models.BooleanField(default=False)
    order_number_series = models.OneToOneField(
        Numberseries,
        on_delete=models.CASCADE
    )
    terms = models.TextField(blank=True, null=True)
    terms_rendered = models.TextField(blank=True, null=True)
    organization_number = models.CharField(
        blank=True, null=True, max_length=50
    )
    contact_email = models.EmailField(blank=True, null=True)

    objects = OrganizationManager()

    def save(self, *kargs, **kwargs):
        if self.terms:
            self.terms_rendered = linebreaksbr(escape(self.terms))
        else:
            self.terms_rendered = ""

        return super().save(*kargs, **kwargs)

    def __str__(self):
        return "{} (ID: {})".format(self.name, self.id)

    def get_payment_settings(self):
        settings = {}

        # Lets just enable offline option for all organizations
        # for now, should be added as an option at a later time.
        settings["offline"] = (
            "rekordkeeper.payment.offline_provider.OfflineProvider",
            {},
        )

        try:
            settings["stripe"] = self.stripeconfiguration.config()
        except ObjectDoesNotExist:
            pass

        try:
            settings["vipps"] = self.vippsconfiguration.config()
        except ObjectDoesNotExist:
            pass

        return settings

from django.test import TestCase
from django.contrib.auth import get_user_model

from rekordkeeper.organizations.models import Organization
from rekordkeeper.test import ClientWithToken


class OrganizationRequestTestCase(TestCase):
    def test_get_organizations(self):
        Organization.objects.create(
            name="Test organization",
        )

        response = self.client.get("/organizations/")
        self.assertEqual(response.status_code, 200)

        self.assertEqual(
            response.data[0]["name"],
            "Test organization",
        )


class OrganizationRestEndpointTestCase(TestCase):
    def test_anonymous_get_on_list(self):
        response = self.client.get("/organizations/")
        self.assertEqual(response.status_code, 200)

    def test_anonymous_post_on_list(self):
        response = self.client.post("/organizations/")
        self.assertEqual(response.status_code, 401)

    def test_anonymous_get_on_detail(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        response = self.client.get(
            "/organizations/{}/".format(organization.id)
        )
        self.assertEqual(response.status_code, 200)

    def test_anonymous_put_on_detail(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        response = self.client.put(
            "/organizations/{}/".format(organization.id)
        )
        self.assertEqual(response.status_code, 401)

    def test_anonymous_delete_on_detail(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        response = self.client.delete(
            "/organizations/{}/".format(organization.id)
        )
        self.assertEqual(response.status_code, 401)

    def test_admin_get_on_list(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        user = get_user_model().objects.create(
            username="testuser",
            organization=organization,
        )

        client = ClientWithToken(user.auth_token.key)
        response = client.get("/organizations/")
        self.assertEqual(response.status_code, 200)

    def test_admin_post_on_list(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        user = get_user_model().objects.create(
            username="testuser",
            organization=organization,
        )

        client = ClientWithToken(user.auth_token.key)
        response = client.post("/organizations/")
        self.assertEqual(response.status_code, 403)

    def test_admin_get_on_detail(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        user = get_user_model().objects.create(
            username="testuser",
            organization=organization,
        )

        client = ClientWithToken(user.auth_token.key)
        response = client.get(
            "/organizations/{}/".format(organization.id)
        )
        self.assertEqual(response.status_code, 200)

    def test_admin_put_on_detail(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        user = get_user_model().objects.create(
            username="testuser",
            organization=organization,
        )

        client = ClientWithToken(user.auth_token.key)
        response = client.put(
            "/organizations/{}/".format(organization.id),
            data={
                "name": "Some other name",
            },
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)

    def test_admin_delete_on_detail(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        user = get_user_model().objects.create(
            username="testuser",
            organization=organization,
        )

        client = ClientWithToken(user.auth_token.key)
        response = client.delete(
            "/organizations/{}/".format(organization.id)
        )
        self.assertEqual(response.status_code, 403)

    def test_different_org_admin_get_on_list(self):
        Organization.objects.create(
            name="Test organization",
        )

        different_organization = Organization.objects.create(
            name="Different test organization",
        )

        different_user = get_user_model().objects.create(
            username="testuser",
            organization=different_organization,
        )

        client = ClientWithToken(different_user.auth_token.key)
        response = client.get("/organizations/")
        self.assertEqual(response.status_code, 200)

    def test_different_org_admin_post_on_list(self):
        Organization.objects.create(
            name="Test organization",
        )

        different_organization = Organization.objects.create(
            name="Different test organization",
        )

        different_user = get_user_model().objects.create(
            username="testuser",
            organization=different_organization,
        )

        client = ClientWithToken(different_user.auth_token.key)
        response = client.post("/organizations/")
        self.assertEqual(response.status_code, 403)

    def test_different_org_admin_get_on_detail(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        different_organization = Organization.objects.create(
            name="Different test organization",
        )

        different_user = get_user_model().objects.create(
            username="testuser",
            organization=different_organization,
        )

        client = ClientWithToken(different_user.auth_token.key)
        response = client.get(
            "/organizations/{}/".format(organization.id)
        )
        self.assertEqual(response.status_code, 200)

    def test_different_org_admin_put_on_detail(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        different_organization = Organization.objects.create(
            name="Different test organization",
        )

        different_user = get_user_model().objects.create(
            username="testuser",
            organization=different_organization,
        )

        client = ClientWithToken(different_user.auth_token.key)
        response = client.put(
            "/organizations/{}/".format(organization.id),
            data={
                "name": "Some other name",
            },
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_different_org_admin_delete_on_detail(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        different_organization = Organization.objects.create(
            name="Different test organization",
        )

        different_user = get_user_model().objects.create(
            username="testuser",
            organization=different_organization,
        )

        client = ClientWithToken(different_user.auth_token.key)
        response = client.delete(
            "/organizations/{}/".format(organization.id)
        )
        self.assertEqual(response.status_code, 403)

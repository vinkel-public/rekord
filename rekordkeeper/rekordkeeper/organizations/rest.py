from rest_framework import viewsets, serializers, routers

from rekordkeeper.organizations.models import Organization
from rekordkeeper.permissions import ReadOnly, UpdaterInOrganization


class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = [
            "id",
            "name",
            "vat_enabled",
            "terms",
            "terms_rendered",
            "organization_number",
        ]
        extra_kwargs = {
            "terms_rendered": {"read_only": True},
        }


class OrganizationViewSet(viewsets.ModelViewSet):
    """
        This resource should enforce permissions such that:
            * All users are allowed to read
            * Organization admin can update
    """
    serializer_class = OrganizationSerializer
    permission_classes = [ReadOnly | UpdaterInOrganization]
    queryset = Organization.objects.all()


router = routers.DefaultRouter()
router.register(r"organizations", OrganizationViewSet, basename="organization")

from django.contrib import admin

from rekordkeeper.organizations.models import Organization


class OrganizationAdmin(admin.ModelAdmin):
    list_display = ("name", "contact_email")


admin.site.register(Organization, OrganizationAdmin)

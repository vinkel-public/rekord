"""rekordkeeper URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

from rekordkeeper.organizations.rest import router as organizations
from rekordkeeper.catalogues.rest import router as products
from rekordkeeper.orders.rest import router as orders
from rekordkeeper.vendors.rest import router as vendors
from rekordkeeper.payment.urls import urlpatterns as payment_urls
from rekordkeeper.payment.rest import router as payment
from rekordkeeper.marketplaces.rest import router as marketplaces
from rekordkeeper.users.urls import urlpatterns as users_urls
from rekordkeeper.reports.urls import urlpatterns as reports_urls
from rekordkeeper.shipping.rest import router as shipping
from rekordkeeper.vat.rest import router as vat


urlpatterns = [
    path("admin/", admin.site.urls),
    path("users/", include(users_urls)),
    path("reports/", include(reports_urls)),
    path("", include(payment_urls)),
    path("", include(organizations.urls)),
    path("", include(products.urls)),
    path("", include(orders.urls)),
    path("", include(vendors.urls)),
    path("", include(payment.urls)),
    path("", include(marketplaces.urls)),
    path("", include(shipping.urls)),
    path("", include(vat.urls)),
]

if settings.DEBUG:
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    )

import os
import json


SECRETS_PATH = os.environ.get(
    "REKORDKEEPER_SECRETS_PATH",
    "/srv/sites/api.rekord.app/secrets.json"
)

_secret_file_data = {}

try:
    with open(SECRETS_PATH) as fd:
        _secret_file_data = json.load(fd)
except FileNotFoundError:
    pass


def get_secret(name):
    try:
        return os.environ["REKORDKEEPER_" + name]
    except KeyError:
        pass

    try:
        return _secret_file_data[name]
    except KeyError:
        pass

    raise Exception(
        "Tried to load secret key '{}' from environment and filesystem "
        "due to being in production mode, but secret was not found"
        .format(name)
    )


def get_environ(name, default):
    return os.environ.get("REKORDKEEPER_" + name, default)


DEBUG = True  # Debug enabled in production for now
SECRET_KEY = get_secret("SECRET_KEY")

DEFAULT_FROM_EMAIL = get_environ("DEFAULT_FROM_EMAIL", "post@rekord.app")

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = "in-v3.mailjet.com"
EMAIL_PORT = 25
EMAIL_HOST_USER = get_secret("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = get_secret("EMAIL_HOST_PASSWORD")
EMAIL_USE_TLS = True

PAYMENT_HOST = get_environ("PAYMENT_HOST", "api.rekord.app")

STATIC_ROOT = get_environ(
    "STATIC_ROOT",
    "/srv/sites/api.rekord.app/public_html/static"
)

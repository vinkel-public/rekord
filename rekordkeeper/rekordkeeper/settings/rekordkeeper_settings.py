INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django_extensions",
    "corsheaders",
    "payments",
    "rest_framework",
    "rest_framework.authtoken",
    "rekordkeeper.organizations",
    "rekordkeeper.numberseries",
    "rekordkeeper.vendors",
    "rekordkeeper.catalogues",
    "rekordkeeper.orders",
    "rekordkeeper.payment",
    "rekordkeeper.marketplaces",
    "rekordkeeper.users",
    "rekordkeeper.reports",
    "rekordkeeper.shipping",
    "rekordkeeper.vat",
]

CORS_ALLOW_ALL_ORIGINS = True

PAYMENT_HOST = "api.demo.rekord.app"
PAYMENT_USES_SSL = True
PAYMENT_MODEL = "payment.payment"
PAYMENT_VARIANTS = {}

STATIC_ROOT = "/srv/sites/api.demo.rekord.app/public_html/static"

AUTH_USER_MODEL = "users.User"

REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": [
        "rest_framework.authentication.TokenAuthentication",
    ]
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

import os
from pathlib import Path

from .default_settings import *  # noqa
from .rekordkeeper_settings import *  # noqa


production_mode = True

if os.environ.get("REKORDKEEPER_MODE", "production") == "development":
    production_mode = False

if production_mode:
    from .production_settings import *  # noqa

elif (Path(__file__).parent / "dev_settings.py").is_file():
    from .dev_settings import *  # noqa

from django.apps import AppConfig


class NumberseriesConfig(AppConfig):
    name = "numberseries"

from django.db import models


class Numberseries(models.Model):
    name = models.CharField(max_length=255)
    start_number = models.IntegerField()
    current_number = models.IntegerField()

    def increment(self):
        myself = Numberseries.objects.select_for_update().get(id=self.id)
        new_number = myself.current_number + 1
        myself.current_number = new_number
        myself.save()
        return new_number

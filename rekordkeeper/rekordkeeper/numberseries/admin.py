from django.contrib import admin

from rekordkeeper.numberseries.models import Numberseries


class NumberseriesAdmin(admin.ModelAdmin):
    list_display = ("name", "start_number", "current_number")


admin.site.register(Numberseries, NumberseriesAdmin)

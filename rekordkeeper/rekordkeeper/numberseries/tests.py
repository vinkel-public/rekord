from django.test import TestCase

from rekordkeeper.numberseries.models import Numberseries


class NumberseriesModelTestCase(TestCase):
    def test_increment_series(self):
        number_series = Numberseries.objects.create(
            name="test series",
            start_number=1,
            current_number=1,
        )
        new_number = number_series.increment()
        self.assertEqual(new_number, 2)

from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.utils.html import escape
from django.template.defaultfilters import linebreaksbr

from rekordkeeper.organizations.models import Organization
from rekordkeeper.shipping.models import DeliveryMethod


class Vendor(models.Model):
    organization = models.ForeignKey(
        Organization,
        on_delete=models.CASCADE,
    )
    name = models.CharField(max_length=256)
    image = models.ImageField(upload_to="vendors", null=True)
    description = models.TextField(blank=True, null=True)
    description_rendered = models.TextField(blank=True, null=True)

    featured = models.BooleanField(default=False)
    vipps_enabled = models.BooleanField(default=False)
    stripe_enabled = models.BooleanField(default=False)
    offline_enabled = models.BooleanField(default=True)
    delivery_methods = models.ManyToManyField(DeliveryMethod, blank=True)

    def __str__(self):
        return "{} (ID: {})".format(
            self.name,
            self.id
        )

    def save(self, *kargs, **kwargs):
        if self.description:
            self.description_rendered = linebreaksbr(escape(self.description))
        else:
            self.description_rendered = ""
        return super().save(*kargs, **kwargs)

    @property
    def vat_enabled(self):
        return self.organization.vat_enabled

    def configured_payment_methods(self):
        methods = []

        try:
            self.organization.offlineconfiguration
        except ObjectDoesNotExist:
            pass
        else:
            methods.append("offline")

        try:
            self.organization.stripeconfiguration
        except ObjectDoesNotExist:
            pass
        else:
            methods.append("stripe")

        try:
            self.organization.vippsconfiguration
        except ObjectDoesNotExist:
            pass
        else:
            methods.append("vipps")

        return methods

    def enabled_payment_methods(self):
        configured_methods = self.configured_payment_methods()
        methods = []

        if self.offline_enabled and "offline" in configured_methods:
            conf = self.organization.offlineconfiguration
            methods.append(
                {
                    "variant": "offline",
                    "frontend_label": conf.frontend_label,
                    "description": conf.description,
                }
            )

        if self.vipps_enabled and "vipps" in configured_methods:
            conf = self.organization.vippsconfiguration
            methods.append(
                {
                    "variant": "vipps",
                    "frontend_label": conf.frontend_label,
                }
            )

        if self.stripe_enabled and "stripe" in configured_methods:
            conf = self.organization.stripeconfiguration
            methods.append(
                {
                    "variant": "stripe",
                    "frontend_label": conf.frontend_label,
                    "public_key": conf.public_key,
                }
            )

        return methods

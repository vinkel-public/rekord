from django.test import TestCase
from django.contrib.auth import get_user_model

from rekordkeeper.vendors.models import Vendor
from rekordkeeper.organizations.models import Organization
from rekordkeeper.test import ClientWithToken


class VendorsRequestTestCase(TestCase):
    def test_get_organizations(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        response = self.client.get("/vendors/")
        self.assertEqual(response.status_code, 200)

        self.assertEqual(
            response.data[0]["name"],
            "Test vendor",
        )

    def test_get_organization(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        response = self.client.get("/vendors/{}/".format(organization.id))
        self.assertEqual(response.status_code, 200)

        self.assertEqual(
            response.data["name"],
            "Test vendor",
        )

    def test_post_requires_auth(self):
        response = self.client.post("/vendors/")
        self.assertEqual(response.status_code, 401)

    def test_post_works_with_authed_user(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        user = get_user_model().objects.create(
            username="testuser",
            organization=organization,
        )
        client = ClientWithToken(user.auth_token.key)

        response = client.post("/vendors/", data={
            "organization": organization.id,
            "name": "Test vendor",
        })
        self.assertEqual(response.status_code, 201)

from rest_framework import viewsets, serializers, routers
from django.core.exceptions import ObjectDoesNotExist

from rekordkeeper.vendors.models import Vendor
from rekordkeeper.permissions import HasRelatedOrganization, ReadOnly, WriterInOrganization


class VendorSerializer(serializers.ModelSerializer):
    enabled_payment_methods = serializers.ListField(read_only=True)
    configured_payment_methods = serializers.ListField(read_only=True)
    stripe_public_key = serializers.SerializerMethodField()

    class Meta:
        model = Vendor
        fields = [
            "id",
            "name",
            "description",
            "description_rendered",
            "organization",
            "enabled_payment_methods",
            "configured_payment_methods",
            "stripe_public_key",
            "vipps_enabled",
            "stripe_enabled",
            "offline_enabled",
            "image",
            "delivery_methods",
            "vat_enabled",
        ]
        extra_kwargs = {
            "vat_enabled": {"read_only": True},
            "description_rendered": {"read_only": True},
        }

    def get_stripe_public_key(self, obj):
        try:
            return obj.organization.stripeconfiguration.public_key
        except ObjectDoesNotExist:
            return None


class VendorViewSet(viewsets.ModelViewSet):
    """
        This resource should enforce permissions such that:
            * All users are allowed to read
            * Authenticated admins in related organization can write
    """
    queryset = Vendor.objects.all()
    serializer_class = VendorSerializer
    permission_classes = [ReadOnly | WriterInOrganization]

    def get_queryset(self):
        organization = self.request.query_params.get("organization", False)
        if organization:
            return Vendor.objects.filter(organization=organization)

        if self.request.query_params.get("featured", "") == "true":
            return Vendor.objects.filter(featured=True)

        return Vendor.objects.all()


router = routers.DefaultRouter()
router.register(r"vendors", VendorViewSet)

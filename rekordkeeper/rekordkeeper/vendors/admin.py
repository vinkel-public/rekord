from django.contrib import admin

from rekordkeeper.vendors.models import Vendor


class VendorAdmin(admin.ModelAdmin):
    list_display = ("id", "organization", "name", "featured")


admin.site.register(Vendor, VendorAdmin)

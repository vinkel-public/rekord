from django.shortcuts import get_object_or_404

from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import PermissionDenied
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)

from rekordkeeper.orders.models import Order
from rekordkeeper.organizations.models import Organization

# TODO: Change this to a class-based view

# This resource should enforce permissions such that:
# * Only read access
# * User is authenticated
# * The organization is related to the requesting user


@api_view(["GET"])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def orders(request, organization):
    if request.user.organization.id != organization:
        raise PermissionDenied()

    organization = get_object_or_404(Organization, pk=organization)
    data = []

    for order in Order.objects.filter(vendor__organization=organization):
        data.append(
            {
                "ordered_at": order.ordered_at,
                "total": order.total,
                "name": order.name,
            }
        )

    return Response(data)

from django.urls import path

from rekordkeeper.reports.rest import orders


urlpatterns = [
    path("orders/<int:organization>/", orders),
]

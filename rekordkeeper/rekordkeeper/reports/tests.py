from django.test import TestCase
from django.contrib.auth import get_user_model

from rekordkeeper.orders.models import Order, OrderLine
from rekordkeeper.vat.models import VatClass
from rekordkeeper.shipping.models import DeliveryMethod
from rekordkeeper.catalogues.models import Product
from rekordkeeper.vendors.models import Vendor
from rekordkeeper.organizations.models import Organization
from rekordkeeper.test import ClientWithToken


class ReportsTestCase(TestCase):
    def test_report_requires_auth(self):
        response = self.client.get("/reports/orders/1/")
        self.assertEqual(response.status_code, 401)

    def test_get_report(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        delivery_method = DeliveryMethod.objects.create(
            name="Test delivery",
            price=0,
            organization=organization,
            vat_class=vat_class,
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        product1 = Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        product2 = Product.objects.create(
            vendor=vendor,
            name="Test product 2",
            price=50,
            vat_class=vat_class,
        )

        order = Order.objects.create(
            vendor=vendor,
            delivery_method=delivery_method,
        )

        OrderLine.objects.create(
            order=order,
            product=product1,
            quantity=1,
        )

        OrderLine.objects.create(
            order=order,
            product=product2,
            quantity=2,
        )

        user = get_user_model().objects.create(
            username="testuser",
            organization=organization,
        )
        user.save()

        client = ClientWithToken(user.auth_token.key)
        response = client.get(
            "/reports/orders/{}/".format(organization.id),
        )
        self.assertEqual(response.status_code, 200)

    def test_report_requires_same_organization(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        wrong_organization = Organization.objects.create(
            name="Another test organization",
        )

        user = get_user_model().objects.create(
            username="testuser",
            organization=organization,
        )
        user.save()

        client = ClientWithToken(user.auth_token.key)
        response = client.get(
            "/reports/orders/{}/".format(wrong_organization.id),
        )
        self.assertEqual(response.status_code, 403)

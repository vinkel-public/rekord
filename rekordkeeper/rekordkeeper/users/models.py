from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import AbstractUser

from rekordkeeper.organizations.models import Organization

from rest_framework.authtoken.models import Token


class User(AbstractUser):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)


@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

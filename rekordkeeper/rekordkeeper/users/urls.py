from django.urls import path

from rekordkeeper.users.rest import CustomAuthToken


urlpatterns = [
    path("login/", CustomAuthToken.as_view()),
]

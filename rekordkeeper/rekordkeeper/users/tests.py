from django.test import TestCase
from django.contrib.auth import get_user_model

from rekordkeeper.organizations.models import Organization


class UsersTestCase(TestCase):
    def test_invalid_login(self):
        response = self.client.post("/users/login/")
        self.assertEqual(response.status_code, 400)

    def test_valid_login(self):
        organization = Organization.objects.create(
            name="Test organization #1",
        )

        password = "testpassword"
        user = get_user_model().objects.create(
            username="testuser",
            organization=organization,
        )
        user.set_password(password)
        user.save()

        response = self.client.post(
            "/users/login/",
            data={
                "username": user.username,
                "password": password,
            },
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)

        data = response.json()

        self.assertEqual(data["token"], user.auth_token.key)
        self.assertEqual(data["name"], "testuser")
        self.assertEqual(data["anonymous"], False)
        self.assertEqual(data["organization"]["name"], organization.name)
        self.assertEqual(data["organization"]["id"], organization.id)

    def test_user_created_with_token(self):
        organization = Organization.objects.create(
            name="Test organization #1",
        )

        user = get_user_model().objects.create(
            username="testuser",
            organization=organization,
        )

        self.assertTrue(user.auth_token.key is not None)

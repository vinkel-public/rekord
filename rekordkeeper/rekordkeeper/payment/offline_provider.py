from payments import RedirectNeeded, PaymentStatus
from payments.core import BasicProvider


class OfflineProvider(BasicProvider):
    """Offline payment provider.

    This provider is suitable for "offline" payments such as cash on
    delivery etc. It does not contain a form and automatically goes
    into "Preauth" status so it allows for "Capture" which would
    mark it as paid.
    """

    def get_form(self, payment, data=None):
        payment.change_status(PaymentStatus.PREAUTH)
        raise RedirectNeeded(payment.get_success_url())

    def process_data(self, payment, request):
        pass

    def capture(self, payment, amount=None):
        payment.change_status(PaymentStatus.CONFIRMED)
        return amount

    def release(self, payment):
        payment.change_status(PaymentStatus.REFUNDED)
        return None

    def refund(self, payment, amount=None):
        payment.change_status(PaymentStatus.REFUNDED)
        return amount or 0

from django.test import TestCase

from rekordkeeper.shipping.models import DeliveryMethod
from rekordkeeper.orders.models import Order, OrderLine
from rekordkeeper.catalogues.models import Product
from rekordkeeper.vat.models import VatClass
from rekordkeeper.vendors.models import Vendor
from rekordkeeper.organizations.models import Organization
from rekordkeeper.payment.models import (
    Payment,
    StripeConfiguration,
    VippsConfiguration,
    OfflineConfiguration,
)


class PaymentsTestCase(TestCase):
    def test_get_payment(self):
        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        organization = Organization.objects.create(
            name="Test organization",
        )

        delivery_method = DeliveryMethod.objects.create(
            name="Test delivery",
            price=0,
            organization=organization,
            vat_class=vat_class,
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        product1 = Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        product2 = Product.objects.create(
            vendor=vendor,
            name="Test product 2",
            price=50,
            vat_class=vat_class,
        )

        order = Order.objects.create(
            vendor=vendor,
            delivery_method=delivery_method,
        )

        OrderLine.objects.create(
            order=order,
            product=product1,
            quantity=1,
        )

        OrderLine.objects.create(
            order=order,
            product=product2,
            quantity=2,
        )

        payment = Payment.objects.create(
            order=order,
            variant=order.payment,
            total=order.total,
            status="confirmed",
        )

        response = self.client.get("/payments/?order={}".format(order.id))
        self.assertEqual(response.status_code, 200)

        self.assertEqual(
            response.data[0]["id"],
            payment.id,
        )

    def test_capture_payment(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        delivery_method = DeliveryMethod.objects.create(
            name="Test delivery",
            price=0,
            organization=organization,
            vat_class=vat_class,
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        product1 = Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        product2 = Product.objects.create(
            vendor=vendor,
            name="Test product 2",
            price=50,
            vat_class=vat_class,
        )

        order = Order.objects.create(
            vendor=vendor,
            payment="offline",
            delivery_method=delivery_method,
        )

        OrderLine.objects.create(
            order=order,
            product=product1,
            quantity=1,
        )

        OrderLine.objects.create(
            order=order,
            product=product2,
            quantity=2,
        )

        payment = Payment.objects.create(
            order=order,
            variant=order.payment,
            total=order.total,
            status="preauth",
        )

        response = self.client.post(
            "/payments/{}/capture/?order={}".format(payment.id, order.id),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)

        payment = Payment.objects.get(id=payment.id)

        self.assertEqual(payment.status, "confirmed")

    def test_refund_payment(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        delivery_method = DeliveryMethod.objects.create(
            name="Test delivery",
            price=0,
            organization=organization,
            vat_class=vat_class,
        )

        product1 = Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        product2 = Product.objects.create(
            vendor=vendor,
            name="Test product 2",
            price=50,
            vat_class=vat_class,
        )

        order = Order.objects.create(
            vendor=vendor,
            payment="offline",
            delivery_method=delivery_method,
        )

        OrderLine.objects.create(
            order=order,
            product=product1,
            quantity=1,
        )

        OrderLine.objects.create(
            order=order,
            product=product2,
            quantity=2,
        )

        payment = Payment.objects.create(
            order=order,
            variant=order.payment,
            total=order.total,
            status="confirmed",
        )

        response = self.client.put(
            "/payments/{}/refund/?order={}".format(payment.id, order.id),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)

        payment = Payment.objects.get(id=payment.id)

        self.assertEqual(payment.status, "refunded")

    def test_post_payment(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        product1 = Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        product2 = Product.objects.create(
            vendor=vendor,
            name="Test product 2",
            price=50,
            vat_class=vat_class,
        )

        delivery_method = DeliveryMethod.objects.create(
            name="Test delivery",
            price=0,
            organization=organization,
            vat_class=vat_class,
        )

        order = Order.objects.create(
            vendor=vendor,
            delivery_method=delivery_method,
        )

        OrderLine.objects.create(
            order=order,
            product=product1,
            quantity=1,
        )

        OrderLine.objects.create(
            order=order,
            product=product2,
            quantity=2,
        )

        self.assertEqual(Payment.objects.count(), 0)

        response = self.client.post(
            "/payments/?order={}".format(order.id),
            data={
                "order": order.id,
            },
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(Payment.objects.count(), 1)

    def test_process_payment(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        product1 = Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        product2 = Product.objects.create(
            vendor=vendor,
            name="Test product 2",
            price=50,
            vat_class=vat_class,
        )

        delivery_method = DeliveryMethod.objects.create(
            name="Test delivery",
            price=0,
            organization=organization,
            vat_class=vat_class,
        )

        order = Order.objects.create(
            vendor=vendor,
            payment="offline",
            delivery_method=delivery_method,
        )

        OrderLine.objects.create(
            order=order,
            product=product1,
            quantity=1,
        )

        OrderLine.objects.create(
            order=order,
            product=product2,
            quantity=2,
        )

        payment = Payment.objects.create(
            order=order,
            variant=order.payment,
            total=order.total,
            status="input",
        )

        response = self.client.post(
            "/payments/{}/process/?order={}".format(payment.id, order.id),
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 200)

        self.assertEqual(response.json()["success"], True)

        payment = Payment.objects.get(id=payment.id)

        self.assertEqual(payment.status, "preauth")

    def test_process_payment_vipps(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        delivery_method = DeliveryMethod.objects.create(
            name="Test delivery",
            price=0,
            organization=organization,
            vat_class=vat_class,
        )

        product1 = Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        product2 = Product.objects.create(
            vendor=vendor,
            name="Test product 2",
            price=50,
            vat_class=vat_class,
        )

        order = Order.objects.create(
            vendor=vendor,
            payment="offline",
            delivery_method=delivery_method,
        )

        OrderLine.objects.create(
            order=order,
            product=product1,
            quantity=1,
        )

        OrderLine.objects.create(
            order=order,
            product=product2,
            quantity=2,
        )

        payment = Payment.objects.create(
            order=order,
            variant=order.payment,
            total=order.total,
            status="input",
        )

        response = self.client.post(
            "/payments/{}/process/?order={}".format(payment.id, order.id),
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 200)

        self.assertEqual(response.json()["success"], True)

        payment = Payment.objects.get(id=payment.id)

        self.assertEqual(payment.status, "preauth")

    def test_get_stripeconfiguration(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        response = self.client.get(
            "/stripeconfigurations/?organization={}".format(organization.id)
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 0)

        StripeConfiguration.objects.create(
            organization=organization,
            name="Stripe testing keys",
            frontend_label="Pay with card",
            secret_key="sk_foobar",
            public_key="pk_foobar",
        )

        response = self.client.get(
            "/stripeconfigurations/?organization={}".format(organization.id)
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 1)

    def test_get_vippsconfiguration(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        response = self.client.get(
            "/vippsconfigurations/?organization={}".format(organization.id)
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 0)

        VippsConfiguration.objects.create(
            organization=organization,
            name="Vipps testing keys",
            frontend_label="Pay with Vipps",
            client_id="client_id",
            client_secret="client_secret",
            subscription_key="123",
            merchant_serial_number="123",
            endpoint="http://localhost/",
        )

        response = self.client.get(
            "/vippsconfigurations/?organization={}".format(organization.id)
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 1)

    def test_get_offlineconfiguration(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        response = self.client.get(
            "/offlineconfigurations/?organization={}".format(organization.id)
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 0)

        OfflineConfiguration.objects.create(
            organization=organization,
            name="Offline method testing",
            frontend_label="Pay upon delivery",
        )

        response = self.client.get(
            "/offlineconfigurations/?organization={}".format(organization.id)
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 1)

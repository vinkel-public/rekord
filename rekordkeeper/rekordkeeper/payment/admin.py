from django.contrib import admin
from rekordkeeper.payment.models import (
    Payment,
    VippsConfiguration,
    StripeConfiguration,
    OfflineConfiguration,
)


class PaymentAdmin(admin.ModelAdmin):
    list_display = (
        "order",
        "variant",
        "status",
        "total",
        "captured_amount",
    )


class VippsConfigurationAdmin(admin.ModelAdmin):
    list_display = (
        "organization",
        "name",
        "endpoint",
    )


class StripeConfigurationAdmin(admin.ModelAdmin):
    list_display = ("organization", "name")


class OfflineConfigurationAdmin(admin.ModelAdmin):
    list_display = ("organization", "name")


admin.site.register(Payment, PaymentAdmin)
admin.site.register(VippsConfiguration, VippsConfigurationAdmin)
admin.site.register(StripeConfiguration, StripeConfigurationAdmin)
admin.site.register(OfflineConfiguration, OfflineConfigurationAdmin)

import requests


class VippsApiException(Exception):
    pass


class VippsClient:
    def __init__(
        self,
        client_id,
        client_secret,
        subscription_key,
        endpoint,
        merchant_serial_number,
        access_token=None,
    ):

        self.client_id = client_id
        self.client_secret = client_secret
        self.subscription_key = subscription_key
        self.endpoint = endpoint
        self.merchant_serial_number = merchant_serial_number

        if access_token:
            self._access_token = access_token

    def get_access_token_headers(self):
        return {
            "client_id": self.client_id,
            "client_secret": self.client_secret,
            "Ocp-Apim-Subscription-Key": self.subscription_key,
        }

    def get_payment_request_body(self, amount=False, transaction_text=False):
        body = {
            "merchantInfo": {
                "merchantSerialNumber": self.merchant_serial_number
            },
            "transaction": {},
        }

        if amount:
            body["transaction"]["amount"] = int(amount * 100)

        if transaction_text:
            body["transaction"]["transactionText"] = transaction_text

        return body

    def get_payment_request_headers(self, order_id=None):
        access_token = self.get_access_token()
        headers = {
            "Content-Type": "application/json",
            "Ocp-Apim-Subscription-Key": self.subscription_key,
            "Authorization": "Bearer {}".format(access_token),
        }

        if order_id:
            headers.update({"orderId": order_id})

        return headers

    def get_access_token(self):
        if hasattr(self, "_access_token"):
            return self._access_token

        url = self.endpoint + "/accesstoken/get"
        headers = self.get_access_token_headers()
        response = requests.post(url, headers=headers)

        self._access_token = response.json()["access_token"]
        return self._access_token

    def get_payment_details(self, order_id):
        url = self.endpoint + "ecomm/v2/payments/{}/details".format(order_id)
        headers = self.get_payment_request_headers()
        response = requests.get(url, headers=headers)

        if response.status_code != 200:
            raise VippsApiException(
                "Vipps API returned status code: {}, body: {}".format(
                    response.status_code, response.json()
                )
            )

        return response.json()

    def create_payment(self, body):
        headers = self.get_payment_request_headers()
        url = self.endpoint + "/ecomm/v2/payments"

        response = requests.post(url, headers=headers, json=body)
        return response

    def cancel_payment(self, order_id, transaction_text="Cancelling payment"):
        headers = self.get_payment_request_headers()
        url = self.endpoint + "ecomm/v2/payments/{}/cancel".format(order_id)
        body = self.get_payment_request_body(transaction_text=transaction_text)
        response = requests.put(url, headers=headers, json=body)

        if response.status_code != 200:
            raise VippsApiException(
                "Vipps API returned status code: {}, body: {}".format(
                    response.status_code, response.json()
                )
            )

        return response

    def refund_payment(
        self, order_id, amount, transaction_text="Refunding payment"
    ):
        headers = self.get_payment_request_headers()
        url = self.endpoint + "ecomm/v2/payments/{}/refund".format(order_id)

        body = self.get_payment_request_body(
            transaction_text=transaction_text, amount=amount
        )

        response = requests.post(url, headers=headers, json=body)

        if response.status_code != 200:
            raise VippsApiException(
                "Vipps API returned status code: {}, body: {}".format(
                    response.status_code, response.json()
                )
            )

        return response

    def capture_payment(
        self, order_id, amount, transaction_text="Capturing payment"
    ):

        headers = self.get_payment_request_headers()
        url = self.endpoint + "/ecomm/v2/payments/{}/capture".format(order_id)

        body = self.get_payment_request_body(
            transaction_text=transaction_text, amount=amount
        )

        response = requests.post(url, headers=headers, json=body)

        if response.status_code != 200:
            raise VippsApiException(
                "Vipps API returned status code: {}, body: {}".format(
                    response.status_code, response.json()
                )
            )

        return response

import json
from decimal import Decimal
from django.conf import settings
from django.http import HttpResponse
from payments import PaymentError, PaymentStatus, RedirectNeeded
from payments.core import BasicProvider
from .client import VippsClient, VippsApiException


class VippsProvider(BasicProvider):
    def __init__(
        self,
        client_id,
        client_secret,
        subscription_key,
        endpoint,
        merchant_serial_number,
        **kwargs
    ):

        self.client = VippsClient(
            client_id,
            client_secret,
            subscription_key,
            endpoint,
            merchant_serial_number,
        )

        super().__init__(**kwargs)

    def hasSuccessfulTransactionEvent(self, history, event):
        return bool(
            list(
                filter(
                    lambda x: x["operation"] == event
                    and x["operationSuccess"],
                    history,
                )
            )
        )

    def check_status(self, payment):
        data = self.client.get_payment_details(payment.token)
        history = data["transactionLogHistory"]
        reserved = self.hasSuccessfulTransactionEvent(history, "RESERVE")
        captured = self.hasSuccessfulTransactionEvent(history, "CAPTURE")
        refunded = self.hasSuccessfulTransactionEvent(history, "REFUND")
        released = self.hasSuccessfulTransactionEvent(history, "VOID")
        return (reserved, captured, refunded, released)

    def process_data(self, payment, request):
        data = json.loads(request.body.decode("utf-8"))
        transaction = data.get("transactionInfo", {})
        status = transaction["status"]
        transaction_id = transaction.get("transactionId", False)
        error = data.get("errorInfo", False)

        if transaction_id:
            payment.transaction_id = transaction_id

        if payment.status == PaymentStatus.INPUT:
            if status == "RESERVED":
                payment.change_status(PaymentStatus.PREAUTH)
            elif status == "REJECTED":
                payment.change_status(PaymentStatus.REJECTED)
        else:
            raise PaymentError("Can only process PaymentStatus.INPUT payments")

        # Lets save the callback data for later use
        extra_data = json.loads(payment.extra_data or "{}")
        extra_data["callback_data"] = data

        if error:
            extra_data["callback_data_error"] = error

        payment.extra_data = json.dumps(extra_data)
        payment.save()

        return HttpResponse()

    def get_form(self, payment, data=None):
        if payment.status == PaymentStatus.WAITING:
            payment.change_status(PaymentStatus.INPUT)

        if payment.status != PaymentStatus.INPUT:
            raise PaymentError("Payment is not in a PaymentStatus.INPUT state")

        response = self.create_payment(payment)

        if response.status_code != 200:
            raise PaymentError(
                "Create payment responded with status code {}".format(
                    response.status_code
                )
            )

        extra_data = json.loads(payment.extra_data or "{}")
        extra_data["create_response"] = response.json()

        payment.extra_data = json.dumps(extra_data)
        payment.save()

        raise RedirectNeeded(response.json()["url"])

    def create_payment(self, payment):
        order_id = payment.token
        transaction_text = "Bestilling fra {}".format(
            payment.order.vendor.name
        )
        transaction_amount = int(payment.total * 100)
        customer_number = payment.order.phone

        if settings.PAYMENT_USES_SSL:
            host = "https://{}".format(settings.PAYMENT_HOST)
        else:
            host = "http://{}".format(settings.PAYMENT_HOST)

        body = {
            "customerInfo": {
                "mobileNumber": customer_number,
            },
            "merchantInfo": {
                "consentRemovalPrefix": "{}/vipps".format(host),
                "callbackPrefix": "{}{}".format(
                    host, payment.get_process_url()
                ),
                "shippingDetailsPrefix": "{}/vipps".format(host),
                "fallBack": payment.get_return_url(),
                "isApp": False,
                "merchantSerialNumber": self.client.merchant_serial_number,
                "paymentType": "eComm Regular Payment",
            },
            "transaction": {
                "amount": transaction_amount,
                "orderId": order_id,
                "timeStamp": payment.order.ordered_at.isoformat(),
                "transactionText": transaction_text,
            },
        }

        response = self.client.create_payment(body)
        return response

    def capture(self, payment, amount=None):
        amount = amount or payment.total

        try:
            self.client.capture_payment(payment.token, amount)
        except VippsApiException:
            raise PaymentError("Unable to capture payment")

        return Decimal(amount)

    def refund(self, payment, amount=None):
        amount = amount or payment.total

        try:
            self.client.refund_payment(payment.token, amount)
        except VippsApiException:
            raise PaymentError("Unable to refund payment")

        return Decimal(amount)

    def release(self, payment, amount=None):
        amount = amount or payment.total

        try:
            self.client.cancel_payment(payment.token)
        except VippsApiException:
            raise PaymentError("Unable to cancel payment")

        return Decimal(amount)

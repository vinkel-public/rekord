from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect

from rekordkeeper.orders.models import Order
from rekordkeeper.payment.models import Payment
from rekordkeeper.vendors.models import Vendor
from payments import get_payment_model, RedirectNeeded, PaymentStatus


def payments_return(request, token):
    payment = get_object_or_404(Payment, token=token)

    return HttpResponseRedirect(
        "{}/#/payment/{}".format(payment.return_origin, payment.id)
    )


def test_payment_return(request, payment_id):
    payment = get_object_or_404(Payment, id=payment_id)

    if payment.status in [PaymentStatus.PREAUTH, PaymentStatus.CONFIRMED]:
        return HttpResponseRedirect(
            "/test-payments/{}/success/".format(payment.id)
        )
    elif payment.status in [PaymentStatus.ERROR, PaymentStatus.REJECTED]:
        return HttpResponseRedirect(
            "/test-payments/{}/failure/".format(payment.id)
        )

    return HttpResponse(
        "We dont have success or failure status yet, status is: {}".format(
            payment.status
        )
    )


def test_payment_failure(request, payment_id):
    return HttpResponse(
        "Sorry but the payment failed! Retry or something i guess?"
    )


def test_payment_success(request, payment_id):
    return HttpResponse("<h1>Success!</h1><p>Thanks for your payment!</p>")


def test_payment(request, payment_id):
    payment = get_object_or_404(get_payment_model(), id=payment_id)

    try:
        form = payment.get_form(data=request.POST or None)
    except RedirectNeeded as redirect_to:
        return HttpResponseRedirect(str(redirect_to))

    tmpl = {
        "form": form,
        "payment": payment,
    }
    return render(request, "payment/test-payment.html", tmpl)


def test_payments(request):

    vendor = Vendor.objects.all()[0]

    if request.method == "POST":
        # Create a dummy order
        order = Order.objects.create(
            vendor=vendor,
            name="Jone Eide",
            phone="40868451",  # Our magic Vipps testing number
            email="jone@idev.no",
            payment="vipps",
        )

        # Generate some orderlines
        for product in vendor.product_set.all():
            order.orderline_set.create(product=product, quantity=1)

        payment = order.create_payment(
            variant=request.POST.get("variant"),
            ip_address=request.META.get("REMOTE_ADDR", "127.0.0.1"),
        )
        return HttpResponseRedirect("/test-payments/{}/".format(payment.id))

    return render(request, "payment/test-payments.html")

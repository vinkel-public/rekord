from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from django.db.transaction import atomic
from django.conf import settings
from django.shortcuts import get_object_or_404
from django.http import Http404

from payments import get_payment_model
from payments.core import provider_factory

from rekordkeeper.payment.views import (
    test_payments,
    test_payment,
    test_payment_success,
    test_payment_failure,
    test_payment_return,
    payments_return,
)


@csrf_exempt
@atomic
def process_data(request, token, provider=None):
    """
    Calls process_data of an appropriate provider.
    Raises Http404 if variant does not exist.
    """
    Payment = get_payment_model()
    payment = get_object_or_404(Payment, token=token)

    # TODO: This might have some unintended consequences
    settings.PAYMENT_VARIANTS = (
        payment.order.vendor.organization.get_payment_settings()
    )

    if not provider:
        try:
            provider = provider_factory(payment.variant)
        except ValueError:
            raise Http404("No such payment")

    return provider.process_data(payment, request)


urlpatterns = [
    path(
        "payments/return/<uuid:token>/",
        payments_return,
        name="payments-return",
    ),
    path("payments/process/<uuid:token>", process_data),
    path("payments/process/vipps/v2/payments/<uuid:token>", process_data),
]


# These views serve as a minimal implementation of django-payments
# to allow us to test the payment flow easily before implementing
# in Vue frontend. Not for production use.
if settings.DEBUG:
    urlpatterns += [
        path("test-payments/", test_payments),
        path("test-payments/<int:payment_id>/", test_payment),
        path("test-payments/<int:payment_id>/success/", test_payment_success),
        path("test-payments/<int:payment_id>/failure/", test_payment_failure),
        path("test-payments/<int:payment_id>/return/", test_payment_return),
    ]

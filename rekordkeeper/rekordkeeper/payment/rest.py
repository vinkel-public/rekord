from rest_framework import viewsets, serializers, routers
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from rest_framework.decorators import action

from payments import RedirectNeeded, PaymentStatus

from rekordkeeper.payment.models import (
    Payment,
    VippsConfiguration,
    StripeConfiguration,
    OfflineConfiguration,
)


class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        exclude = ["currency", "variant"]

    def create(self, validated_data):
        order = validated_data["order"]
        validated_data["variant"] = order.payment
        validated_data["total"] = order.total
        validated_data["currency"] = "NOK"
        return Payment.objects.create(**validated_data)


class PaymentViewSet(viewsets.ModelViewSet):
    serializer_class = PaymentSerializer

    @action(detail=True, methods=["post"])
    def process(self, request, pk):
        payment = self.get_object()
        response = {
            "success": False,
        }

        # We might actually come into this view after the
        # user has been redirected to a third party gateway
        # to complete payment. Lets check for some status
        # which indicates that and return accordingly.
        if payment.status in [PaymentStatus.PREAUTH, PaymentStatus.CONFIRMED]:
            response["success"] = True
            return Response(response)

        if payment.status in [PaymentStatus.REJECTED, PaymentStatus.ERROR]:
            return Response(response)

        # Fire django-payments internal payment processing step,
        # unfortunately named get_form which is a bit misleading
        try:
            payment.get_form(None)
        except RedirectNeeded as redirect_to:
            # Since get_form raises RedirectNeeded in success/failure
            # modes, we need to check for these as we only want to
            # return a redirect url in the scenario where our payment
            # provider requires an external redirect.
            if str(redirect_to) == payment.get_success_url():
                response["success"] = True
            elif str(redirect_to) == payment.get_failure_url():
                pass
            else:
                response["success"] = True
                response["redirect"] = str(redirect_to)

        # Okay get_form did not raise RedirectNeeded,
        # lets inspect payment.status instead
        if payment.status in [PaymentStatus.PREAUTH, PaymentStatus.CONFIRMED]:
            response["success"] = True

        return Response(response)

    @action(detail=True, methods=["post"])
    def capture(self, request, pk):
        payment = self.get_object()
        payment.capture()
        return Response({})

    @action(detail=True, methods=["put"])
    def refund(self, request, pk):
        payment = self.get_object()
        payment.refund()
        return Response({})

    @action(detail=True, methods=["post"])
    def release(self, request, pk):
        payment = self.get_object()
        payment.release()
        return Response({})

    def get_queryset(self):
        order_id = self.request.query_params.get("order")

        if order_id is not None:
            return Payment.objects.filter(order_id=order_id)
        elif "pk" in self.kwargs:
            return Payment.objects.filter(id=self.kwargs["pk"])

        raise ValidationError("order required")


router = routers.DefaultRouter()
router.register(r"payments", PaymentViewSet, basename="payments")


class StripeConfigurationSerializer(serializers.ModelSerializer):
    class Meta:
        model = StripeConfiguration
        fields = "__all__"


class StripeConfigurationViewSet(viewsets.ModelViewSet):
    serializer_class = StripeConfigurationSerializer

    def get_queryset(self):
        organization_id = self.request.query_params.get("organization")

        if organization_id is not None:
            return StripeConfiguration.objects.filter(
                organization=organization_id
            )

        raise ValidationError("Organization required")


router.register(
    r"stripeconfigurations",
    StripeConfigurationViewSet,
    basename="stripeconfigurations",
)


class VippsConfigurationSerializer(serializers.ModelSerializer):
    class Meta:
        model = VippsConfiguration
        fields = "__all__"


class VippsConfigurationViewSet(viewsets.ModelViewSet):
    serializer_class = VippsConfigurationSerializer

    def get_queryset(self):
        organization_id = self.request.query_params.get("organization")

        if organization_id is not None:
            return VippsConfiguration.objects.filter(
                organization=organization_id
            )

        raise ValidationError("Organization required")


router.register(
    r"vippsconfigurations",
    VippsConfigurationViewSet,
    basename="vippsconfigurations",
)


class OfflineConfigurationSerializer(serializers.ModelSerializer):
    class Meta:
        model = OfflineConfiguration
        fields = "__all__"


class OfflineConfigurationViewSet(viewsets.ModelViewSet):
    serializer_class = OfflineConfigurationSerializer

    def get_queryset(self):
        organization_id = self.request.query_params.get("organization")

        if organization_id is not None:
            return OfflineConfiguration.objects.filter(
                organization=organization_id
            )

        raise ValidationError("Organization required")


router.register(
    r"offlineconfigurations",
    OfflineConfigurationViewSet,
    basename="offlineconfigurations",
)

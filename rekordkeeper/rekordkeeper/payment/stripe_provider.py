import stripe

from payments.stripe import StripeProvider as OriginalStripeProvider
from payments import PaymentStatus, RedirectNeeded


class StripeProvider(OriginalStripeProvider):
    def get_form(self, payment, data=None):

        if payment.status == PaymentStatus.WAITING:
            payment.change_status(PaymentStatus.INPUT)

        stripe.api_key = self.secret_key

        try:
            charge_data = {
                "capture": False,
                "amount": int(payment.total * 100),
                "currency": payment.currency,
                "card": payment.stripe_token,
                "description": "Bestilling fra {}".format(
                    payment.order.vendor.name
                ),
            }

            self.charge = stripe.Charge.create(**charge_data)
        except stripe.error.CardError as e:
            # The card has been declined
            payment.change_status(PaymentStatus.ERROR, str(e))
            raise RedirectNeeded(payment.get_failure_url())
        else:
            payment.transaction_id = self.charge.id
            payment.save()

        # Things worked out well?
        payment.change_status(PaymentStatus.PREAUTH)
        raise RedirectNeeded(payment.get_success_url())

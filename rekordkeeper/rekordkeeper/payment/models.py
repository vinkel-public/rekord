from django.db import models
from django.conf import settings
from django.urls import reverse

from rekordkeeper.orders.models import Order
from rekordkeeper.organizations.models import Organization

from payments.models import BasePayment
from payments import PaymentStatus
from payments.core import provider_factory


class Payment(BasePayment):
    order = models.ForeignKey(
        Order, related_name="orderpayment", on_delete=models.CASCADE
    )

    return_origin = models.CharField(null=True, max_length=1024)
    stripe_token = models.CharField(null=True, max_length=200)

    def change_status(self, status, *kargs, **kwargs):
        # Check if we are moving from a waiting state to a
        # successful payment state
        if status == PaymentStatus.PREAUTH and self.status in [
            PaymentStatus.INPUT,
            PaymentStatus.WAITING,
        ]:
            # Seems like we are, lets send the email receipt
            # as well as give the order a number.
            self.order.set_order_number()
            self.order.send_email_receipt()

        super().change_status(status, *kargs, **kwargs)

    def get_form(self, data=None):

        # TODO: This might have some unintended consequences
        settings.PAYMENT_VARIANTS = (
            self.order.vendor.organization.get_payment_settings()
        )

        provider = provider_factory(self.variant)
        return provider.get_form(self, data=data)

    def get_process_url(self):
        if self.variant == "vipps":
            return "/payments/process/vipps"

        return super().get_process_url()

    def get_return_url(self):
        protocol = "https://" if settings.PAYMENT_USES_SSL else "http://"
        return "{}{}{}".format(
            protocol,
            settings.PAYMENT_HOST,
            reverse("payments-return", args=[self.token]),
        )

    def get_failure_url(self):
        return "http://{}/test-payments/{}/failure/".format(
            settings.PAYMENT_HOST, self.id
        )

    def get_success_url(self):
        return "http://{}/test-payments/{}/success/".format(
            settings.PAYMENT_HOST, self.id
        )

    def get_purchased_items(self):
        # Not in use for Vipps or Stripe
        return []

    def refund(self, *kargs, **kwargs):
        # TODO: This might have some unintended consequences
        settings.PAYMENT_VARIANTS = (
            self.order.vendor.organization.get_payment_settings()
        )

        super().refund(*kargs, **kwargs)
        settings.PAYMENT_VARIANTS = {}

    def release(self, *kargs, **kwargs):
        # TODO: This might have some unintended consequences
        settings.PAYMENT_VARIANTS = (
            self.order.vendor.organization.get_payment_settings()
        )

        super().release(*kargs, **kwargs)
        settings.PAYMENT_VARIANTS = {}

    def capture(self, *kargs, **kwargs):
        # TODO: This might have some unintended consequences
        settings.PAYMENT_VARIANTS = (
            self.order.vendor.organization.get_payment_settings()
        )

        super().capture(*kargs, **kwargs)
        settings.PAYMENT_VARIANTS = {}


class VippsConfiguration(models.Model):
    organization = models.OneToOneField(Organization, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    frontend_label = models.CharField(max_length=100)
    client_id = models.CharField(max_length=255)
    client_secret = models.CharField(max_length=255)
    subscription_key = models.CharField(max_length=255)
    merchant_serial_number = models.CharField(max_length=255)
    endpoint = models.CharField(max_length=255)

    def config(self):
        return (
            "rekordkeeper.payment.vipps.VippsProvider",
            {
                "client_id": self.client_id,
                "client_secret": self.client_secret,
                "subscription_key": self.subscription_key,
                "merchant_serial_number": self.merchant_serial_number,
                "endpoint": self.endpoint,
            },
        )


class StripeConfiguration(models.Model):
    organization = models.OneToOneField(Organization, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    frontend_label = models.CharField(max_length=100)
    secret_key = models.CharField(max_length=255)
    public_key = models.CharField(max_length=255)

    def config(self):
        return (
            "rekordkeeper.payment.stripe_provider.StripeProvider",
            {"secret_key": self.secret_key, "public_key": self.public_key},
        )


class OfflineConfiguration(models.Model):
    organization = models.OneToOneField(Organization, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    frontend_label = models.CharField(max_length=100)
    description = models.TextField(null=True)

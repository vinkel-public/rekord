from django.db import models

from rekordkeeper.organizations.models import Organization
from rekordkeeper.vat.models import VatClass


class DeliveryMethod(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=500, null=True, blank=True)
    requires_delivery_address = models.BooleanField(default=False)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    vat_class = models.ForeignKey(VatClass, on_delete=models.CASCADE)

import json

from django.test import TestCase
from django.contrib.auth import get_user_model

from rekordkeeper.organizations.models import Organization
from rekordkeeper.vat.models import VatClass
from rekordkeeper.test import ClientWithToken


class OrdersTestCase(TestCase):
    def test_create_deliverymethod(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        response = self.client.get(
            "/deliverymethods/?organization={}".format(organization.id)
        )

        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(response.data), 0)

        user = get_user_model().objects.create(
            username="testuser",
            organization=organization,
        )
        user.save()

        client = ClientWithToken(user.auth_token.key)

        response = client.post(
            "/deliverymethods/?organization={}".format(organization.id),
            data=json.dumps(
                {
                    "organization": organization.id,
                    "name": "Levering på Reko",
                    "description": None,
                    "requires_delivery_address": False,
                    "vat_class": vat_class.id,
                }
            ),
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 201)
        self.assertEqual(organization.deliverymethod_set.count(), 1)

        # Make sure API returns one method for this org as well
        response = self.client.get(
            "/deliverymethods/?organization={}".format(organization.id)
        )

        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(response.data), 1)

    def test_get_organization_deliverymethod(self):
        organization1 = Organization.objects.create(
            name="Test organization",
        )

        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        organization1.deliverymethod_set.create(
            name="Henting på Reko-Ring",
            price=0,
            requires_delivery_address=False,
            vat_class=vat_class,
        )

        organization1.deliverymethod_set.create(
            name="Sendes med posten",
            price=100,
            requires_delivery_address=True,
            vat_class=vat_class,
        )

        vendor1 = organization1.vendor_set.create(name="Butikk 1")

        # Setup another org
        organization2 = Organization.objects.create(
            name="Test organization 2",
        )

        organization2.deliverymethod_set.create(
            name="Henting på Reko-Ring for org 2",
            price=0,
            requires_delivery_address=False,
            vat_class=vat_class,
        )

        organization2.deliverymethod_set.create(
            name="Sendes med posten for org 2",
            price=100,
            requires_delivery_address=True,
            vat_class=vat_class,
        )

        vendor2 = organization1.vendor_set.create(name="Butikk 2")
        vendor2.delivery_methods.add(organization2.deliverymethod_set.all()[0])

        # Org 1 should have 2 delivery methods
        response = self.client.get(
            "/deliverymethods/?organization={}".format(organization1.id)
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 2)

        # Vendor 1 should have 0 delivery methods
        response = self.client.get(
            "/deliverymethods/?organization={}&vendor={}".format(
                organization1.id, vendor1.id
            )
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 0)

        # Vendor 2 should have 1 delivery methods
        response = self.client.get(
            "/deliverymethods/?organization={}&vendor={}".format(
                organization2.id, vendor2.id
            )
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)

    def test_anonymous_post_denied(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        response = self.client.post(
            "/deliverymethods/?organization={}".format(organization.id),
            data={
                "organization": organization.id,
                "name": "Levering på Reko",
                "description": None,
                "requires_delivery_address": False,
                "vat_class": vat_class.id,
            },
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 401)

from rest_framework import viewsets, serializers, routers
from rest_framework.exceptions import ValidationError

from rekordkeeper.shipping.models import DeliveryMethod
from rekordkeeper.vendors.models import Vendor
from rekordkeeper.permissions import HasRelatedOrganization, ReadOnly


class DeliveryMethodSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeliveryMethod
        fields = "__all__"


class DeliveryMethodViewSet(viewsets.ModelViewSet):
    """
        This resource should enforce permissions such that:
            * All users are allowed to read
            * Authenticated admins in related organization can write
    """

    serializer_class = DeliveryMethodSerializer
    permission_classes = [HasRelatedOrganization | ReadOnly]

    def get_queryset(self):
        organization_id = self.request.query_params.get("organization")
        vendor_id = self.request.query_params.get("vendor")

        if organization_id is None:
            raise ValidationError("organization required")

        delivery_methods = DeliveryMethod.objects.filter(
            organization_id=organization_id
        )

        if vendor_id is not None:
            vendor = Vendor.objects.get(id=vendor_id)
            delivery_methods = delivery_methods.filter(
                id__in=[m.id for m in vendor.delivery_methods.all()]
            )

        return delivery_methods


router = routers.DefaultRouter()
router.register(
    r"deliverymethods",
    DeliveryMethodViewSet,
    basename="deliverymethod",
)

from django.contrib import admin

from rekordkeeper.shipping.models import DeliveryMethod


class DeliveryMethodAdmin(admin.ModelAdmin):
    list_display = ('organization', 'name', 'price', 'vat_class')


admin.site.register(DeliveryMethod, DeliveryMethodAdmin)

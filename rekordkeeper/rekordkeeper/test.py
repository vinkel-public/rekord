# from django.test import TestCase
from django.test.client import Client


class ClientWithToken(Client):
    def __init__(self, token, **kwargs):
        self.token = token
        super().__init__(**kwargs)

    def generic(self, *kargs, **kwargs):
        kwargs["HTTP_AUTHORIZATION"] = "Token {}".format(self.token)
        return super().generic(*kargs, **kwargs)


# class RestEndpointTestCase(TestCase):
#     def test_anonymous_get_on_list(self):
#         raise NotImplementedError("Test case must be overriden")
#
#     def test_anonymous_post_on_list(self):
#         raise NotImplementedError("Test case must be overriden")
#
#     def test_anonymous_get_on_detail(self):
#         raise NotImplementedError("Test case must be overriden")
#
#     def test_anonymous_put_on_detail(self):
#         raise NotImplementedError("Test case must be overriden")
#
#     def test_anonymous_delete_on_detail(self):
#         raise NotImplementedError("Test case must be overriden")
#
#     def test_admin_get_on_list(self):
#         raise NotImplementedError("Test case must be overriden")
#
#     def test_admin_post_on_list(self):
#         raise NotImplementedError("Test case must be overriden")
#
#     def test_admin_get_on_detail(self):
#         raise NotImplementedError("Test case must be overriden")
#
#     def test_admin_put_on_detail(self):
#         raise NotImplementedError("Test case must be overriden")
#
#     def test_admin_delete_on_detail(self):
#         raise NotImplementedError("Test case must be overriden")
#
#     def test_different_org_admin_get_on_list(self):
#         raise NotImplementedError("Test case must be overriden")
#
#     def test_different_org_admin_post_on_list(self):
#         raise NotImplementedError("Test case must be overriden")
#
#     def test_different_org_admin_get_on_detail(self):
#         raise NotImplementedError("Test case must be overriden")
#
#     def test_different_org_admin_put_on_detail(self):
#         raise NotImplementedError("Test case must be overriden")
#
#     def test_different_org_admin_delete_on_detail(self):
#         raise NotImplementedError("Test case must be overriden")

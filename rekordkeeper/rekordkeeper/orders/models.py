from os import urandom
from binascii import hexlify

from django.db import models
from django.conf import settings
from django.template.loader import render_to_string
from django.core.mail import EmailMessage

from rekordkeeper.vendors.models import Vendor
from rekordkeeper.shipping.models import DeliveryMethod
from rekordkeeper.catalogues.models import Product
from rekordkeeper.vat.models import VatClass

from payments import get_payment_model
from decimal import Decimal


class OrderManager(models.Manager):
    def create(self, *kargs, **kwargs):
        # We are overriding this to copy some fields to convert shipping
        # method price and name at time of order into separate fields.
        kwargs["delivery_method_name"] = kwargs["delivery_method"].name
        kwargs["delivery_method_price"] = kwargs["delivery_method"].price
        kwargs["delivery_method_vat_class"] = kwargs[
            "delivery_method"
        ].vat_class
        kwargs["delivery_method_vat_class_percentage"] = kwargs[
            "delivery_method"
        ].vat_class.percentage
        kwargs["vat_enabled"] = kwargs["vendor"].organization.vat_enabled
        return super().create(*kargs, **kwargs)


class Order(models.Model):
    vendor = models.ForeignKey(
        Vendor,
        on_delete=models.CASCADE,
    )
    order_number = models.IntegerField(blank=True, null=True)
    ordered_at = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=256)
    phone = models.CharField(max_length=256)
    email = models.CharField(max_length=256)
    payment = models.CharField(max_length=256)
    delivered = models.BooleanField(default=False)

    delivery_method = models.ForeignKey(
        DeliveryMethod, on_delete=models.CASCADE
    )
    delivery_method_name = models.CharField(
        max_length=100, blank=True, null=True
    )
    delivery_method_price = models.DecimalField(
        max_digits=10, decimal_places=2, blank=True, null=True
    )
    delivery_method_vat_class = models.ForeignKey(
        VatClass, on_delete=models.CASCADE
    )

    delivery_method_vat_class_percentage = models.PositiveSmallIntegerField()

    delivery_address = models.CharField(max_length=500, blank=True, null=True)
    delivery_zip = models.CharField(max_length=100, blank=True, null=True)
    delivery_region = models.CharField(max_length=100, blank=True, null=True)
    vat_enabled = models.BooleanField(default=False)

    access_code = models.BinaryField(max_length=64)

    objects = OrderManager()

    @property
    def has_delivery_address(self):
        return "" not in [
            self.delivery_address,
            self.delivery_zip,
            self.delivery_region
        ]

    def send_email_receipt(self):
        subject = "Ordrebekreftelse {}: {}".format(
            self.vendor.name,
            self.order_number,
        )
        body = render_to_string(
            "orders/order-email-receipt.txt",
            {"order": self}
        )

        if self.vendor.organization.contact_email:
            reply_to = self.vendor.organization.contact_email
            bcc = [self.vendor.organization.contact_email]
        else:
            reply_to = settings.DEFAULT_FROM_EMAIL
            bcc = []

        message = EmailMessage(
            subject=subject,
            body=body,
            from_email=settings.DEFAULT_FROM_EMAIL,
            to=[self.email],
            reply_to=[reply_to],
            bcc=bcc,
        )

        message.send()

    class Meta:
        ordering = ["-ordered_at"]

    def set_order_number(self):
        # Lets use our Organization order numberseries
        # to generate our order number
        org = self.vendor.organization
        self.order_number = org.order_number_series.increment()
        self.save()

    @property
    def total(self):
        return (
            Decimal(sum([x.total for x in self.orderline_set.all()]))
            + self.delivery_method_price
        )

    @property
    def vat_total(self):
        return (
            Decimal(sum([x.vat_total for x in self.orderline_set.all()]))
            + self.delivery_method_vat_total
        )

    @property
    def payment_status(self):
        try:
            return self.orderpayment.all()[0].status
        except IndexError:
            return "payment_not_found"

    @property
    def total_excluding_vat(self):
        return self.total - self.vat_total

    @property
    def delivery_method_vat_total(self):
        if not self.vat_enabled:
            return Decimal(0)

        return round(
            self.delivery_method_price
            / (100 + Decimal(self.delivery_method_vat_class_percentage))
            * Decimal(self.delivery_method_vat_class_percentage),
            2,
        )

    @property
    def delivery_method_excluding_vat(self):
        return self.delivery_method_price - self.delivery_method_vat_total

    def create_payment(self, variant, ip_address="127.0.0.1"):

        first_name, last_name = self.name.split(" ")
        tax = Decimal(0)
        delivery = Decimal(0)
        currency = "NOK"

        PaymentModel = get_payment_model()
        payment = PaymentModel.objects.create(
            order=self,
            variant=variant,
            description="Ordre {} fra {}".format(self.id, self.vendor.name),
            total=Decimal(self.total),
            tax=tax,
            currency=currency,
            delivery=delivery,
            billing_first_name=first_name,
            billing_last_name=last_name,
            billing_address_1="",
            billing_address_2="",
            billing_city="",
            billing_postcode="",
            billing_country_code="NO",
            billing_country_area="",
            customer_ip_address=ip_address,
        )

        return payment

    def save(self, *kargs, **kwargs):
        # Ensure that we have a value for self.access_code
        if not self.access_code:
            self.access_code = hexlify(urandom(32))

        return super().save(*kargs, **kwargs)


class OrderLineManager(models.Manager):
    def create(self, *kargs, **kwargs):
        # We are overriding this to copy some fields to convert product
        # price and name at time of order into separate fields.
        kwargs["product_price"] = kwargs["product"].price
        kwargs["product_name"] = kwargs["product"].name
        kwargs["vat_class"] = kwargs["product"].vat_class
        kwargs["vat_class_percentage"] = kwargs["vat_class"].percentage
        return super().create(*kargs, **kwargs)


class OrderLine(models.Model):
    order = models.ForeignKey(
        Order,
        on_delete=models.CASCADE,
    )
    product = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
    )
    product_name = models.CharField(max_length=256)
    product_price = models.DecimalField(max_digits=12, decimal_places=2)

    vat_class = models.ForeignKey(VatClass, on_delete=models.CASCADE)
    vat_class_percentage = models.PositiveSmallIntegerField()

    quantity = models.PositiveIntegerField()

    objects = OrderLineManager()

    @property
    def total(self):
        return self.product_price * self.quantity

    @property
    def vat_total(self):
        if not self.order.vat_enabled:
            return Decimal(0)

        return round(
            self.total
            / (100 + self.vat_class_percentage)
            * self.vat_class_percentage,
            2,
        )

    @property
    def total_excluding_vat(self):
        return self.total - self.vat_total

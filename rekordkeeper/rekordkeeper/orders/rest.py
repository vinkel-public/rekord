from rest_framework import viewsets, serializers, routers
from rest_framework.exceptions import ValidationError

from rekordkeeper.orders.models import Order, OrderLine


class OrderSerializer(serializers.ModelSerializer):
    payment_status = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = [
            "id",
            "vendor",
            "ordered_at",
            "name",
            "phone",
            "email",
            "payment",
            "total",
            "delivered",
            "payment_status",
            "delivery_method_name",
            "delivery_method_price",
            "delivery_method",
            "delivery_address",
            "delivery_zip",
            "delivery_region",
            "vat_enabled",
            "vat_total",
            "total_excluding_vat",
            "order_number",
        ]
        extra_kwargs = {
            "vat_enabled": {"read_only": True},
            "delivery_method_name": {"read_only": True},
            "delivery_method_price": {"read_only": True},
        }

    def get_payment_status(self, obj):
        try:
            return obj.orderpayment.all()[0].status
        except IndexError:
            return "error_not_found"

    def create(self, validated_data):
        order = super().create(validated_data)
        order.delivery_method_name = order.delivery_method.name
        order.delivery_method_price = order.delivery_method.price
        order.save()
        return order


class OrderViewSet(viewsets.ModelViewSet):
    serializer_class = OrderSerializer

    def get_queryset(self):
        vendor_id = self.request.query_params.get("vendor")
        if vendor_id is not None:
            return Order.objects.filter(vendor_id=vendor_id)

        return Order.objects.all()


class OrderLineSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderLine
        fields = "__all__"
        extra_kwargs = {
            "product_name": {"read_only": True},
            "product_price": {"read_only": True},
            "vat_class": {"read_only": True},
            "vat_class_percentage": {"read_only": True},
        }


class OrderLineViewSet(viewsets.ModelViewSet):
    serializer_class = OrderLineSerializer

    def get_queryset(self):
        order_id = self.request.query_params.get("order")
        if order_id is not None:
            return OrderLine.objects.filter(order_id=order_id)

        raise ValidationError("order required")


router = routers.DefaultRouter()
router.register(r"orders", OrderViewSet, basename="order")
router.register(r"orderlines", OrderLineViewSet, basename="orderline")

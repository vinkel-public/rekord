from django.contrib import admin
from rekordkeeper.orders.models import Order


class OrderAdmin(admin.ModelAdmin):
    list_display = ("vendor", "order_number", "ordered_at", "name", "phone", "email", "payment", "payment_status")


admin.site.register(Order, OrderAdmin)

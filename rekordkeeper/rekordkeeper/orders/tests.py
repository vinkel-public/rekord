import pytest

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.core.management import call_command

from rekordkeeper.orders.models import Order, OrderLine
from rekordkeeper.catalogues.models import Product
from rekordkeeper.vat.models import VatClass
from rekordkeeper.vendors.models import Vendor
from rekordkeeper.organizations.models import Organization
from rekordkeeper.shipping.models import DeliveryMethod
from rekordkeeper.test import ClientWithToken


class OrdersTestCase(TestCase):
    def test_create_simplest_order(self):
        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        organization = Organization.objects.create(
            name="Test organization",
        )

        delivery_method = DeliveryMethod.objects.create(
            name="Test delivery",
            price=0,
            organization=organization,
            vat_class=vat_class,
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        product1 = Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        product2 = Product.objects.create(
            vendor=vendor,
            name="Test product 2",
            price=50,
            vat_class=vat_class,
        )

        order = Order.objects.create(
            vendor=vendor,
            delivery_method=delivery_method,
        )

        OrderLine.objects.create(
            order=order,
            product=product1,
            quantity=1,
        )

        OrderLine.objects.create(
            order=order,
            product=product2,
            quantity=2,
        )


class OrdersRequestTestCase(TestCase):
    def test_get_orders(self):
        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        organization = Organization.objects.create(
            name="Test organization",
        )

        delivery_method = DeliveryMethod.objects.create(
            name="Test delivery",
            price=0,
            organization=organization,
            vat_class=vat_class,
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        Product.objects.create(
            vendor=vendor,
            name="Test product 2",
            price=50,
            vat_class=vat_class,
        )

        order = Order.objects.create(
            vendor=vendor,
            delivery_method=delivery_method,
        )

        self.user = get_user_model().objects.create(
            username="testuser",
            organization=organization,
        )

        client = ClientWithToken(self.user.auth_token.key)

        response = client.get("/orders/?vendor={}".format(vendor.id))
        self.assertEqual(response.status_code, 200)

        self.assertEqual(
            response.data[0]["id"],
            order.id,
        )

        self.assertEqual(response.data[0]["total"], 0)

    def test_post_order(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        delivery_method = DeliveryMethod.objects.create(
            name="Test delivery",
            organization=organization,
            price=0,
            vat_class=vat_class,
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        product1 = Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        product2 = Product.objects.create(
            vendor=vendor,
            name="Test product 2",
            price=50,
            vat_class=vat_class,
        )

        response = self.client.post(
            "/orders/?vendor={}".format(vendor.id),
            data={
                "vendor": vendor.id,
                "name": "Ola Nordmann",
                "phone": "42123242",
                "email": "ola@nordmann.com",
                "payment": "vipps",
                "delivery_method": delivery_method.id,
            },
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

        order_id = response.data["id"]

        response = self.client.post(
            "/orderlines/?order={}".format(order_id),
            data={
                "order": order_id,
                "product": product1.id,
                "quantity": 1,
            },
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 201)

        response = self.client.post(
            "/orderlines/?order={}".format(order_id),
            data={
                "order": order_id,
                "product": product2.id,
                "quantity": 2,
            },
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 201)

        self.assertEqual(
            OrderLine.objects.filter(order_id=order_id).count(),
            2,
        )

        order = Order.objects.get(id=order_id)
        self.assertEqual(order.total, 200)

    @pytest.mark.skip("Better tests are coming in another branch")
    def test_anonymous_get_orders_denied(self):
        response = self.client.get("/orders/")
        self.assertEqual(response.status_code, 401)

    def test_anonymous_get_order_with_access_code(self):
        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        organization = Organization.objects.create(
            name="Test organization",
        )

        delivery_method = DeliveryMethod.objects.create(
            name="Test delivery",
            price=0,
            organization=organization,
            vat_class=vat_class,
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        product1 = Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        product2 = Product.objects.create(
            vendor=vendor,
            name="Test product 2",
            price=50,
            vat_class=vat_class,
        )

        order = Order.objects.create(
            vendor=vendor,
            delivery_method=delivery_method,
        )

        OrderLine.objects.create(
            order=order,
            product=product1,
            quantity=1,
        )

        OrderLine.objects.create(
            order=order,
            product=product2,
            quantity=2,
        )

        response = self.client.get(
            "/orders/{}/?access_code={}".format(order.id, order.access_code)
        )
        self.assertEqual(response.status_code, 200)


class OrdersManagementCommandsTestCase(TestCase):
    def test_create_dummy_data(self):
        self.assertEqual(Organization.objects.all().count(), 0)
        call_command("create_dummy_data")
        self.assertNotEqual(Organization.objects.all().count(), 0)

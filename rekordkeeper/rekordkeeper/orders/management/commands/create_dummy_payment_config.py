import argparse
import json

from django.core.management.base import BaseCommand

from rekordkeeper.organizations.models import Organization
from rekordkeeper.payment.models import VippsConfiguration, StripeConfiguration


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("FILE", type=argparse.FileType())

    def handle(self, *args, **options):
        data = json.load(options["FILE"])

        for org in Organization.objects.all():

            StripeConfiguration.objects.create(
                organization=org, **data["stripe"]
            )

            VippsConfiguration.objects.create(
                organization=org, **data["vipps"]
            )

            for vendor in org.vendor_set.all():
                vendor.stripe_enabled = True
                vendor.vipps_enabled = True
                vendor.save()

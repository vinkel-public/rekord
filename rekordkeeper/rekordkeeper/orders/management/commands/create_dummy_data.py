from django.db import transaction
from django.core.management.base import BaseCommand
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.files.base import ContentFile

from decimal import Decimal

import random

from rekordkeeper.organizations.models import Organization
from rekordkeeper.payment.models import OfflineConfiguration
from rekordkeeper.marketplaces.models import Marketplace
from rekordkeeper.vat.models import VatClass
from rekordkeeper.vendors.models import Vendor


class Command(BaseCommand):
    def create_vat_classes(self):
        self.vat_classes = [
            VatClass.objects.create(name="Ikke momspliktig", percentage=0),
            VatClass.objects.create(name="Generell sats", percentage=25),
            VatClass.objects.create(name="Næringsmidler", percentage=15),
            VatClass.objects.create(
                name="Persontransport, kinobilletter, utleie av rom",
                percentage=12,
            ),
        ]

        self.no_vat_class = self.vat_classes[0]
        self.food_vat_class = self.vat_classes[2]
        self.general_vat_class = self.vat_classes[1]

    def create_organization(
        self,
        name,
        delivery_methods_reko=False,
        vat_enabled=False,
        contact_email="contact-email@example.com",
    ):
        org = Organization.objects.create(
            name=name,
            vat_enabled=vat_enabled,
            contact_email=contact_email,
            organization_number="12345678 MVA",
        )

        if delivery_methods_reko:
            # Setup some default delivery methods
            delivery_methods = [
                {
                    "name": "Hentes på Reko-Ringen Amanda",
                    "description": None,
                    "price": 0,
                    "vat_class": self.general_vat_class,
                },
                {
                    "name": "Hentes på Reko-Ringen Sentrum",
                    "description": None,
                    "price": 0,
                    "vat_class": self.general_vat_class,
                },
            ]
        else:
            # Setup some default delivery methods
            delivery_methods = [
                {
                    "name": "Hentes utenfor butikken",
                    "description": None,
                    "price": 0,
                    "vat_class": self.general_vat_class,
                },
                {
                    "name": "Levert hjem på døren",
                    "description": None,
                    "price": Decimal(100),
                    "requires_delivery_address": True,
                    "vat_class": self.general_vat_class,
                },
            ]

        for delivery_method in delivery_methods:
            org.deliverymethod_set.create(**delivery_method)

        return org

    def create_vendor_with_org(
        self,
        org_name,
        vendor_name,
        featured=False,
        image=False,
        vat_enabled=False,
        delivery_methods_reko=False,
        contact_email="kontakt@example.com",
    ):
        org = self.create_organization(
            org_name,
            vat_enabled=vat_enabled,
            delivery_methods_reko=delivery_methods_reko,
            contact_email=contact_email,
        )
        vendor = org.vendor_set.create(featured=featured, name=vendor_name)

        # Enable all org delivery methods
        vendor.delivery_methods.set(
            [method for method in org.deliverymethod_set.all()]
        )

        OfflineConfiguration.objects.create(
            organization=org,
            name="Dummy offline payment config",
            frontend_label="Kontant ved levering",
            description="Husk å ta med kontanter ;-)",
        )

        if image:
            image_path = (
                settings.BASE_DIR
                / "rekordkeeper"
                / "vendors"
                / "assets"
                / image
            )

            if image_path.is_file():
                with open(image_path, "rb") as fp:
                    image_data = fp.read()

                vendor.image.save(image, ContentFile(image_data))

        vendor.save()

        return vendor

    def create_marketplace(self):
        marketplace = Marketplace.objects.create(
            name="Norheim Sentrum",
            description="Markedsplass for butikkene på Norheim.",
        )

        image = "marketplace-food.jpg"
        image_path = (
            settings.BASE_DIR
            / "rekordkeeper"
            / "marketplaces"
            / "assets"
            / image
        )

        if image_path.is_file():
            with open(image_path, "rb") as fp:
                image_data = fp.read()

            marketplace.image.save(image, ContentFile(image_data))

        vendors = [
            self.create_vendor_with_org(
                "Norheim Bakeri",
                "Norheim Bakeri",
                image="vendor-bakery.jpg",
                contact_email="norheim@example.com",
            ),
            self.create_vendor_with_org(
                "Norheim Ølutsalg",
                "Norheim Ølutsalg",
                image="vendor-beer.jpg",
                contact_email="norheim@example.com",
            ),
            self.create_vendor_with_org(
                "Karmsund Blomster",
                "Karmsund Blomster",
                image="vendor-flowers.jpg",
                contact_email="karmsund@example.com",
            ),
        ]

        # Bakery products
        vendors[0].product_set.create(
            name="Dagens bakevarer",
            description="En samling av bakevarer fra dagens produksjon",
            price=100,
            vat_class=self.food_vat_class,
        )

        vendors[0].product_set.create(
            name="Lunsjpakke for bedrifter",
            description=(
                "Lunsjpakke for bedrift, per person. Bestill ønsket antall."
            ),
            price=60,
            vat_class=self.food_vat_class,
        )

        vendors[0].product_set.create(
            name="Ferske rundstykker",
            description="Rustikke rundstykker, selgers i pakker på 5 stk",
            price=50,
            vat_class=self.food_vat_class,
        )

        # Beer products
        vendors[1].product_set.create(
            name="Gavepakke liten",
            description=(
                "Liten gavepakke inneholder 3 flasker øl og "
                "passende snacks. Leveres innpakket i gaveeske."
            ),
            price=199,
            vat_class=self.food_vat_class,
        )

        vendors[1].product_set.create(
            name="Gavepakke stor",
            description=(
                "Stor gavepakke inneholder 7 flasker øl og passende "
                "salt snacks. Leveres innpakket i gaveeske"
            ),
            price=499,
            vat_class=self.food_vat_class,
        )

        vendors[1].product_set.create(
            name="Salt Snackspakke",
            description="Assortert salt snacks fra vårt rikholdige utvalg.",
            price=99,
            vat_class=self.food_vat_class,
        )

        # Flower shop products
        vendors[2].product_set.create(
            name="Påskebukett liten",
            description="En liten påskebukett med flotte gule farger.",
            price=199,
            vat_class=self.food_vat_class,
        )

        vendors[2].product_set.create(
            name="Påskebukett stor",
            description="En stor påskebukett med flotte gule farger.",
            price=299,
            vat_class=self.food_vat_class,
        )

        vendors[2].product_set.create(
            name="Begravelsesbukett",
            description=(
                "Ring oss på 33333333 for detaljer "
                "etter du har fullført din bestilling."
            ),
            price=399,
            vat_class=self.food_vat_class,
        )

        marketplace.vendors.set(vendors)

    def create_marketplace_reko(self):
        marketplace = Marketplace.objects.create(
            name="Reko-Ringen Norheim",
            description="En markedsplass for butikkene i Norheim sentrum",
        )

        image = "marketplace-reko.jpg"
        image_path = (
            settings.BASE_DIR
            / "rekordkeeper"
            / "marketplaces"
            / "assets"
            / image
        )

        if image_path.is_file():
            with open(image_path, "rb") as fp:
                image_data = fp.read()

            marketplace.image.save(image, ContentFile(image_data))

        vendors = [
            self.create_vendor_with_org(
                "Eide Gaard",
                "Eide Gaard",
                image="vendor-farm.jpg",
                delivery_methods_reko=True,
                contact_email="eide@example.com",
            ),
            self.create_vendor_with_org(
                "Vestlandsk Jakt",
                "Vestlandsk Jakt",
                image="vendor-hunting.jpg",
                delivery_methods_reko=True,
                contact_email="vestlandsk@example.com",
            ),
        ]

        ramslok = vendors[0].product_set.create(
            name="Fermentert ramsløk",
            description="Glass, ca 100g.",
            price=50,
            vat_class=self.food_vat_class,
        )

        ramslok_image_filename = "product-ramslok.jpg"
        ramslok_image_path = (
            settings.BASE_DIR
            / "rekordkeeper"
            / "catalogues"
            / "assets"
            / ramslok_image_filename
        )

        with open(ramslok_image_path, "rb") as fd:
            ramslok.image.save(ramslok_image_filename, ContentFile(fd.read()))

        vendors[0].product_set.create(
            name="Brett med egg, hvite",
            description="30 egg fra frittgående høner på gården.",
            price=70,
            vat_class=self.food_vat_class,
        )

        vendors[0].product_set.create(
            name="Eplemost flaske, 1 liter",
            description="Produsert fra eplene i hagen.",
            price=100,
            vat_class=self.food_vat_class,
        )

        vendors[1].product_set.create(
            name="10kg hjortekjøtt, fryst",
            description="5kg kjøttdeig og 5kg assortert kjøttstykker "
            "(indrefilet, ytrefilet, bankebiff, m.m.).",
            price=2000,
            vat_class=self.food_vat_class,
        )

        vendors[1].product_set.create(
            name="1kg indrefilet av hjort",
            description="Fantastisk mørt kjøtt fra årets hjortejakt.",
            price=500,
            vat_class=self.food_vat_class,
        )

        vendors[1].product_set.create(
            name="1kg kjøttdeig av hjort",
            description="Perfekt for ekstra god taco, shephards pai m.m.",
            price=100,
            vat_class=self.food_vat_class,
        )

        marketplace.vendors.set(vendors)

    def create_featured_vendor_school(self):

        vendor = self.create_vendor_with_org(
            "Rossabø Skole",
            "Rossabø Skole 6B",
            featured=True,
            image="vendor-school-vertical.jpg",
            contact_email="rossabo@example.com",
        )

        vendor.product_set.create(
            name="Doner 50 kroner",
            description="50 kroner til klasseklassen.",
            price=50,
            vat_class=self.no_vat_class,
        )

        vendor.product_set.create(
            name="Doner 100 kroner",
            description="100 kroner til klassekassen.",
            price=100,
            vat_class=self.no_vat_class,
        )

    def create_featured_vendor_soccer_club(self):
        vendor = self.create_vendor_with_org(
            "Nord Idrettslag",
            "Nord Idrettslag Gutter 13",
            featured=True,
            image="vendor-soccer-club.jpg",
            contact_email="nord@example.com",
        )

        vendor.product_set.create(
            name="Årskontingent 2021 spiller",
            description="",
            price=500,
            vat_class=self.general_vat_class,
        )

        vendor.product_set.create(
            name="Årskontingent 2021 familie",
            description="",
            price=1000,
            vat_class=self.general_vat_class,
        )

        vendor.product_set.create(
            name="Klubbdrakter 2021",
            description="Nye drakter med oppdaterte sponsorer for 2021.",
            price=700,
            vat_class=self.general_vat_class,
        )

    def create_featured_vendor_restaurant(self):
        vendor = self.create_vendor_with_org(
            "To Glass Restaurant",
            "To Glass Restaurant",
            featured=True,
            image="vendor-restaurant.jpg",
            contact_email="to@example.com",
        )

        vendor.product_set.create(
            name="Dagens fisk",
            description="Dagens fisk med poteter og gulrøtter.",
            price=119,
            vat_class=self.food_vat_class,
        )

        vendor.product_set.create(
            name="Dagens kjøtt",
            description="Dagens kjøtt med poteter og gulrøtter.",
            price=119,
            vat_class=self.food_vat_class,
        )

    def create_featured_vendor_bar(self):
        pass

    def create_featured_vendor_retailer(self):
        pass

    def create_featured_vendor_with_vat(self):
        vendor = self.create_vendor_with_org(
            "Momsbedriften",
            "Momsbutikken",
            featured=True,
            image="vendor-fish.jpg",
            vat_enabled=True,
            contact_email="momsbedriften@example.com",
        )

        vendor.product_set.create(
            name="Dagens fisk",
            description="Dagens fisk med poteter og gulrøtter.",
            price=119,
            vat_class=self.food_vat_class,
        )

        vendor.product_set.create(
            name="Dagens kjøtt",
            description="Dagens kjøtt med poteter og gulrøtter.",
            price=119,
            vat_class=self.food_vat_class,
        )

    def create_user(self):
        org = Organization.objects.all()[0]
        user = get_user_model().objects.create(
            username="admin",
            organization=org,
            is_superuser=True,
            is_staff=True,
        )
        user.set_password("admin")
        user.save()

        # Set up the same auth token each time for dummy admin user
        # This enables us to recreate the data without loosing auth
        # state on a client

        # Changing PK creates duplicates, so we delete first
        user.auth_token.delete()
        user.auth_token.key = "auth-token-created-by-dummy-data"
        user.auth_token.save()

    def get_random_customer(self):
        customers = [
            {
                "name": "Berit Johansen",
                "phone": "450123123",
                "email": "bente@example.com",
                "delivery_address": "Skreveien 183",
                "delivery_zip": "5542",
                "delivery_region": "Kolnes",
            },
            {
                "name": "Kari Olsen",
                "phone": "49494949",
                "email": "kari@example.com",
                "delivery_address": "Spannavegen 5",
                "delivery_zip": "5531",
                "delivery_region": "Haugesund",
            },
            {
                "name": "Ola Nordmann",
                "phone": "48484848",
                "email": "ole@example.com",
                "delivery_address": "Espevikvegen 5",
                "delivery_zip": "5532",
                "delivery_region": "Haugesund",
            },
            {
                "name": "Petter Eide",
                "phone": "33223322",
                "email": "ole@example.com",
                "delivery_address": "Fjellvegen 5",
                "delivery_zip": "5532",
                "delivery_region": "Haugesund",
            },
        ]

        return random.choice(customers)

    def populate_order_customer_data(self, order):

        customer = self.get_random_customer()

        order.name = customer["name"]
        order.phone = customer["phone"]
        order.email = customer["email"]

        if order.delivery_method.requires_delivery_address:
            order.delivery_address = customer["delivery_address"]
            order.delivery_zip = customer["delivery_zip"]
            order.delivery_region = customer["delivery_region"]

        order.save()

    def create_order_for_vendor(self, vendor, delivery_method):
        payment_method = vendor.enabled_payment_methods()[0]
        order = vendor.order_set.create(
            vendor=vendor,
            payment=payment_method["variant"],
            delivery_method=delivery_method,
        )

        for product in vendor.product_set.all():
            order.orderline_set.create(product=product, quantity=1)

        self.populate_order_customer_data(order)

        order.orderpayment.create(
            variant=order.payment,
            total=order.total,
            currency="NOK",
            status="preauth",
        )

        order.set_order_number()

    def create_orders(self):
        for vendor in Vendor.objects.all():
            for delivery_method in vendor.delivery_methods.all():
                self.create_order_for_vendor(vendor, delivery_method)

    def handle(self, *args, **options):
        with transaction.atomic():
            self.create_vat_classes()
            self.create_marketplace()
            self.create_marketplace_reko()
            self.create_featured_vendor_with_vat()
            self.create_featured_vendor_school()
            self.create_featured_vendor_restaurant()
            self.create_featured_vendor_soccer_club()
            self.create_featured_vendor_bar()
            self.create_featured_vendor_retailer()
            self.create_user()
            self.create_orders()

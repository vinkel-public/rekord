import argparse
import json
from decimal import Decimal

from django.db import transaction
from django.core.management.base import BaseCommand
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.files.base import ContentFile

from rekordkeeper.organizations.models import Organization
from rekordkeeper.payment.models import OfflineConfiguration
from rekordkeeper.vat.models import VatClass
from rekordkeeper.payment.models import VippsConfiguration, StripeConfiguration


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("FILE", type=argparse.FileType())

    def create_vat_classes(self):
        self.vat_classes = [
            VatClass.objects.create(name="Ikke momspliktig", percentage=0),
            VatClass.objects.create(name="Generell sats", percentage=25),
            VatClass.objects.create(name="Næringsmidler", percentage=15),
            VatClass.objects.create(
                name="Persontransport, kinobilletter, utleie av rom",
                percentage=12,
            ),
        ]

        self.no_vat_class = self.vat_classes[0]
        self.food_vat_class = self.vat_classes[2]
        self.general_vat_class = self.vat_classes[1]

    def create_organization(
        self,
        name,
        delivery_methods_reko=False,
        vat_enabled=False,
        contact_email="contact-email@example.com",
    ):
        org = Organization.objects.create(
            name=name, vat_enabled=vat_enabled, contact_email=contact_email
        )

        if delivery_methods_reko:
            # Setup some default delivery methods
            delivery_methods = [
                {
                    "name": "Hentes på Reko-Ringen Amanda",
                    "description": None,
                    "price": 0,
                    "vat_class": self.general_vat_class,
                },
                {
                    "name": "Hentes på Reko-Ringen Sentrum",
                    "description": None,
                    "price": 0,
                    "vat_class": self.general_vat_class,
                },
            ]
        else:
            # Setup some default delivery methods
            delivery_methods = [
                {
                    "name": "Hentes utenfor butikken",
                    "description": None,
                    "price": 0,
                    "vat_class": self.general_vat_class,
                },
                {
                    "name": "Levert hjem på døren",
                    "description": None,
                    "price": Decimal(100),
                    "requires_delivery_address": True,
                    "vat_class": self.general_vat_class,
                },
            ]

        for delivery_method in delivery_methods:
            org.deliverymethod_set.create(**delivery_method)

        return org

    def create_vendor_with_org(
        self,
        org_name,
        vendor_name,
        featured=False,
        image=False,
        vat_enabled=False,
        delivery_methods_reko=False,
        contact_email="kontakt@example.com",
    ):
        org = self.create_organization(
            org_name,
            vat_enabled=vat_enabled,
            delivery_methods_reko=delivery_methods_reko,
            contact_email=contact_email,
        )
        vendor = org.vendor_set.create(featured=featured, name=vendor_name)

        # Enable all org delivery methods
        vendor.delivery_methods.set(
            [method for method in org.deliverymethod_set.all()]
        )

        OfflineConfiguration.objects.create(
            organization=org,
            name="Dummy offline payment config",
            frontend_label="Kontant ved levering",
            description="Husk å ta med kontanter ;-)",
        )

        if image:
            image_path = (
                settings.BASE_DIR
                / "rekordkeeper"
                / "vendors"
                / "assets"
                / image
            )

            if image_path.is_file():
                with open(image_path, "rb") as fp:
                    image_data = fp.read()

                vendor.image.save(image, ContentFile(image_data))

        vendor.save()

        return vendor

    def create_user(self, admin_password):
        org = Organization.objects.all()[0]
        user = get_user_model().objects.create(
            username="admin",
            organization=org,
            is_superuser=True,
            is_staff=True,
        )
        user.set_password(admin_password)
        user.save()

    def handle(self, *args, **options):
        data = json.load(options["FILE"])

        with transaction.atomic():
            self.create_vat_classes()

            gaard = self.create_vendor_with_org(
                "Eide Gaard",
                "Eide Gaard",
                featured=True,
                image="vendor-farm.jpg",
                delivery_methods_reko=True,
                contact_email="eide@example.com",
            )

            ramslok = gaard.product_set.create(
                name="Fermentert ramsløk",
                description="Glass, ca 100g.",
                price=2,
                vat_class=self.food_vat_class,
            )

            ramslok_image_filename = "product-ramslok.jpg"
            ramslok_image_path = (
                settings.BASE_DIR
                / "rekordkeeper"
                / "catalogues"
                / "assets"
                / ramslok_image_filename
            )

            with open(ramslok_image_path, "rb") as fd:
                ramslok.image.save(
                    ramslok_image_filename, ContentFile(fd.read())
                )

            self.create_user(data["admin_password"])

            for org in Organization.objects.all():
                StripeConfiguration.objects.create(
                    organization=org, **data["stripe"]
                )

                VippsConfiguration.objects.create(
                    organization=org, **data["vipps"]
                )

                for vendor in org.vendor_set.all():
                    vendor.stripe_enabled = True
                    vendor.vipps_enabled = True
                    vendor.save()

from django.test import TestCase
from django.contrib.auth import get_user_model

from rekordkeeper.vendors.models import Vendor
from rekordkeeper.organizations.models import Organization
from rekordkeeper.marketplaces.models import Marketplace
from rekordkeeper.test import ClientWithToken


class OrdersTestCase(TestCase):
    def setUp(self):
        self.organizations = [
            Organization.objects.create(
                name="Test organization #1",
            ),
            Organization.objects.create(
                name="Test organization #2",
            ),
        ]

        self.vendors = [
            Vendor.objects.create(
                name="Vendor #1 org #1", organization=self.organizations[0]
            ),
            Vendor.objects.create(
                name="Vendor #2 org #2", organization=self.organizations[1]
            ),
        ]

        self.user = get_user_model().objects.create(
            username="testuser",
            organization=self.organizations[0],
        )
        self.user.save()

    def test_post_marketplace(self):

        self.assertEqual(Marketplace.objects.count(), 0)

        client = ClientWithToken(self.user.auth_token.key)
        response = client.post(
            "/marketplaces/",
            data={
                "name": "Marketplace",
                "description": "Our vendors are awesome",
                "vendors": [self.vendors[0].id, self.vendors[1].id],
            },
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

        self.assertEqual(Marketplace.objects.count(), 1)

    def test_get_marketplace(self):

        self.assertEqual(Marketplace.objects.count(), 0)

        mp = Marketplace.objects.create(
            **{
                "name": "Marketplace",
                "description": "Our vendors are awesome",
            }
        )

        mp.vendors.set([self.vendors[0], self.vendors[1]])

        response = self.client.get(
            "/marketplaces/",
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 1)
        self.assertEqual(response.json()[0]["id"], mp.id)

    def test_post_requires_auth(self):
        response = self.client.post("/marketplaces/")
        self.assertEqual(response.status_code, 401)

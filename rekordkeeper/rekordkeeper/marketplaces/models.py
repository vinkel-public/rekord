from django.db import models

from rekordkeeper.vendors.models import Vendor


class Marketplace(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(null=True)
    vendors = models.ManyToManyField(Vendor)
    image = models.ImageField(upload_to="marketplaces", null=True)

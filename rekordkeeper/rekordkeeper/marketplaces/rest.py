from rest_framework import viewsets, serializers, routers
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from rekordkeeper.marketplaces.models import Marketplace


class MarketplaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Marketplace
        fields = "__all__"
        depth = 1


class MarketplaceViewSet(viewsets.ModelViewSet):
    """
        This resource should enforce permissions such that:
            * All users are allowed to read
            * Authenticated admins can write
    """
    serializer_class = MarketplaceSerializer
    queryset = Marketplace.objects.all()
    permission_classes = [IsAuthenticatedOrReadOnly]


router = routers.DefaultRouter()
router.register(r"marketplaces", MarketplaceViewSet, basename="marketplace")

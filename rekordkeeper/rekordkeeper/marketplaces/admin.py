from django.contrib import admin

from rekordkeeper.marketplaces.models import Marketplace


class MarketplaceAdmin(admin.ModelAdmin):
    list_display = ("name", "description")


admin.site.register(Marketplace, MarketplaceAdmin)

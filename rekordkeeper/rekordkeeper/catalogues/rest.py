from rest_framework import viewsets, serializers, routers
from rest_framework.exceptions import ValidationError, ParseError
from rest_framework.decorators import action
from rest_framework.response import Response

from rekordkeeper.catalogues.models import Product
from rekordkeeper.permissions import HasRelatedVendor, ReadOnly


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = "__all__"


class ProductViewSet(viewsets.ModelViewSet):
    """
        This resource should enforce permissions such that:
            * All users are allowed to read
            * Authenticated admins in related organization can write
    """

    serializer_class = ProductSerializer
    permission_classes = [HasRelatedVendor | ReadOnly]

    def get_queryset(self):
        vendor_id = self.request.query_params.get("vendor")
        if vendor_id is not None:
            return Product.objects.filter(vendor_id=vendor_id)

        raise ValidationError("vendor required")

    @action(detail=True, methods=["post"])
    def delete_image(self, request, pk=None):
        product = self.get_object()
        product.image.delete()

        serializer = self.get_serializer(product)
        return Response(serializer.data)

    @action(detail=True, methods=["post"])
    def upload_image(self, request, pk=None):
        product = self.get_object()

        try:
            image = request.data["productimage"]
        except KeyError:
            raise ParseError("Request has no image file attached")

        product.image = image
        product.save()

        serializer = self.get_serializer(product)
        return Response(serializer.data)


router = routers.DefaultRouter()
router.register(r"products", ProductViewSet, basename="product")

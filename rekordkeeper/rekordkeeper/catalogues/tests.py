from django.test import TestCase
from django.contrib.auth import get_user_model

from rekordkeeper.catalogues.models import Product
from rekordkeeper.vendors.models import Vendor
from rekordkeeper.organizations.models import Organization
from rekordkeeper.vat.models import VatClass
from rekordkeeper.test import ClientWithToken


class CatalogueRequestTestCase(TestCase):
    def test_get_catalogues(self):

        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        organization = Organization.objects.create(
            name="Test organization",
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        response = self.client.get("/products/?vendor={}".format(vendor.id))
        self.assertEqual(response.status_code, 200)

        self.assertEqual(
            response.data[0]["name"],
            "Test product 1",
        )

    def test_anonymous_get_on_detail(self):
        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        organization = Organization.objects.create(
            name="Test organization",
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        response = self.client.get(
            "/products/{}/?vendor={}".format(organization.id, vendor.id)
        )
        self.assertEqual(response.status_code, 200)

    def test_post_requires_auth(self):
        response = self.client.post("/products/")
        self.assertEqual(response.status_code, 401)

    def test_post_with_valid_user(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        user = get_user_model().objects.create(
            username="testuser",
            organization=organization,
        )

        client = ClientWithToken(user.auth_token.key)

        response = client.post(
            "/products/?vendor={}".format(vendor.id),
            data={
                "vendor": vendor.id,
                "name": "Test product 1",
                "description": "Test product",
                "price": 100,
                "vat_class": vat_class.id,
            },
        )

        self.assertEqual(response.status_code, 201)


class RestEndpointTestCase(TestCase):
    def test_anonymous_get_on_list(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        response = self.client.get("/products/?vendor={}".format(vendor.id))
        self.assertEqual(response.status_code, 200)

    def test_anonymous_post_on_list(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        response = self.client.post("/products/?vendor={}".format(vendor.id))
        self.assertEqual(response.status_code, 401)

    def test_anonymous_get_on_detail(self):
        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        organization = Organization.objects.create(
            name="Test organization",
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        product = Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        response = self.client.get(
            "/products/{}/?vendor={}".format(product.id, vendor.id)
        )
        self.assertEqual(response.status_code, 200)

    def test_anonymous_put_on_detail(self):
        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        organization = Organization.objects.create(
            name="Test organization",
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        product = Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        response = self.client.put(
            "/products/{}/?vendor={}".format(product.id, vendor.id),
            data={
                "vendor": vendor.id,
                "name": "new name",
                "description": "Test product",
                "price": 100,
                "vat_class": vat_class.id,
            },
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 401)

    def test_anonymous_delete_on_detail(self):
        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        organization = Organization.objects.create(
            name="Test organization",
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        product = Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        response = self.client.delete(
            "/products/{}/?vendor={}".format(product.id, vendor.id)
        )
        self.assertEqual(response.status_code, 401)

    def test_admin_get_on_list(self):
        organization = Organization.objects.create(
            name="Test organization",
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        user = get_user_model().objects.create(
            username="testuser",
            organization=organization,
        )

        client = ClientWithToken(user.auth_token.key)

        response = client.get("/products/?vendor={}".format(vendor.id))
        self.assertEqual(response.status_code, 200)

    def test_admin_post_on_list(self):
        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        organization = Organization.objects.create(
            name="Test organization",
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        user = get_user_model().objects.create(
            username="testuser",
            organization=organization,
        )

        client = ClientWithToken(user.auth_token.key)

        response = client.post(
            "/products/?vendor={}".format(vendor.id),
            data={
                "vendor": vendor.id,
                "name": "Test product 1",
                "description": "Test product",
                "price": 100,
                "vat_class": vat_class.id,
            },
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 201)

    def test_admin_get_on_detail(self):
        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        organization = Organization.objects.create(
            name="Test organization",
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        product = Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        user = get_user_model().objects.create(
            username="testuser",
            organization=organization,
        )

        client = ClientWithToken(user.auth_token.key)

        response = client.get(
            "/products/{}/?vendor={}".format(product.id, vendor.id)
        )
        self.assertEqual(response.status_code, 200)

    def test_admin_put_on_detail(self):
        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        organization = Organization.objects.create(
            name="Test organization",
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        product = Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        user = get_user_model().objects.create(
            username="testuser",
            organization=organization,
        )

        client = ClientWithToken(user.auth_token.key)

        response = client.put(
            "/products/{}/?vendor={}".format(product.id, vendor.id),
            data={
                "vendor": vendor.id,
                "name": "new name",
                "description": "Test product",
                "price": 100,
                "vat_class": vat_class.id,
            },
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)

    def test_admin_delete_on_detail(self):
        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        organization = Organization.objects.create(
            name="Test organization",
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        product = Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        user = get_user_model().objects.create(
            username="testuser",
            organization=organization,
        )

        client = ClientWithToken(user.auth_token.key)

        response = client.delete(
           "/products/{}/?vendor={}".format(product.id, vendor.id)
        )
        self.assertEqual(response.status_code, 403)

    def test_different_org_admin_get_on_list(self):
        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        organization = Organization.objects.create(
            name="Test organization",
        )

        different_organization = Organization.objects.create(
            name="Different test organization",
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        different_user = get_user_model().objects.create(
            username="testuser",
            organization=different_organization,
        )

        client = ClientWithToken(different_user.auth_token.key)

        response = client.get("/products/?vendor={}".format(vendor.id))
        self.assertEqual(response.status_code, 200)

    def test_different_org_admin_post_on_list(self):
        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        organization = Organization.objects.create(
            name="Test organization",
        )

        different_organization = Organization.objects.create(
            name="Different test organization",
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        different_user = get_user_model().objects.create(
            username="testuser",
            organization=different_organization,
        )

        client = ClientWithToken(different_user.auth_token.key)

        response = client.post(
            "/products/?vendor={}".format(vendor.id),
            data={
                "vendor": vendor.id,
                "name": "Test product 1",
                "description": "Test product",
                "price": 100,
                "vat_class": vat_class.id,
            },
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_different_org_admin_get_on_detail(self):
        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        organization = Organization.objects.create(
            name="Test organization",
        )

        different_organization = Organization.objects.create(
            name="Different test organization",
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        product = Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        different_user = get_user_model().objects.create(
            username="testuser",
            organization=different_organization,
        )

        client = ClientWithToken(different_user.auth_token.key)

        response = client.get(
            "/products/{}/?vendor={}".format(product.id, vendor.id)
        )
        self.assertEqual(response.status_code, 200)

    def test_different_org_admin_put_on_detail(self):
        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        organization = Organization.objects.create(
            name="Test organization",
        )

        different_organization = Organization.objects.create(
            name="Different test organization",
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        product = Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        different_user = get_user_model().objects.create(
            username="testuser",
            organization=different_organization,
        )

        client = ClientWithToken(different_user.auth_token.key)

        response = client.put(
            "/products/{}/?vendor={}".format(product.id, vendor.id),
            data={
                "vendor": vendor.id,
                "name": "new name",
                "description": "Test product",
                "price": 100,
                "vat_class": vat_class.id,
            },
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 403)

    def test_different_org_admin_delete_on_detail(self):
        vat_class = VatClass.objects.create(name="Test class", percentage=0)

        organization = Organization.objects.create(
            name="Test organization",
        )

        different_organization = Organization.objects.create(
            name="Different test organization",
        )

        vendor = Vendor.objects.create(
            organization=organization,
            name="Test vendor",
        )

        product = Product.objects.create(
            vendor=vendor,
            name="Test product 1",
            price=100,
            vat_class=vat_class,
        )

        different_user = get_user_model().objects.create(
            username="testuser",
            organization=different_organization,
        )

        client = ClientWithToken(different_user.auth_token.key)

        response = client.delete(
            "/products/{}/?vendor={}".format(product.id, vendor.id)
        )
        self.assertEqual(response.status_code, 403)

from django.db import models

from rekordkeeper.vendors.models import Vendor
from rekordkeeper.vat.models import VatClass


class Product(models.Model):
    vendor = models.ForeignKey(
        Vendor,
        on_delete=models.CASCADE,
    )
    name = models.CharField(max_length=256)
    description = models.CharField(max_length=10000)
    price = models.DecimalField(max_digits=12, decimal_places=2)
    vat_class = models.ForeignKey(VatClass, on_delete=models.CASCADE)
    image = models.ImageField(upload_to="products", null=True)

from rest_framework.permissions import BasePermission, SAFE_METHODS


class DefaultClosedBasePermission(BasePermission):
    def has_permission(self, request, view):
        return False

    def has_object_permission(self, request, view, obj):
        return False


class ReadOnly(DefaultClosedBasePermission):
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS

    def has_object_permission(self, request, view, obj):
        return request.method in SAFE_METHODS


class UpdaterInOrganization(DefaultClosedBasePermission):
    def has_permission(self, request, view):
        if (
            not request.user.is_anonymous and
            request.method == "PUT"
        ):
            return True

        return False

    def has_object_permission(self, request, view, obj):
        return request.user.organization == obj


class WriterInOrganization(DefaultClosedBasePermission):
    def has_permission(self, request, view):
        # Request is authenticated user trying to write,
        # we need to ensure new data relates to users existing organization
        if (
            not request.user.is_anonymous and
            request.method not in SAFE_METHODS and
            request.user.organization.id == int(request.data["organization"])
        ):
            return True

        return False

    def has_object_permission(self, request, view, obj):
        return False


class HasRelatedOrganization(DefaultClosedBasePermission):
    def has_permission(self, request, view):
        if (
            not request.user.is_anonymous and
            request.method not in SAFE_METHODS
        ):
            # Request is authenticated user trying to write,
            # we need to ensure new data relates to users existing organization
            return request.user.organization.id == int(request.data["organization"])

        return False

    def has_object_permission(self, request, view, obj):
        return request.user.organization == obj.organization


class HasRelatedVendor(DefaultClosedBasePermission):
    def has_permission(self, request, view):
        if (
            not request.user.is_anonymous and
            request.method not in SAFE_METHODS and
            "vendor" in request.data
        ):
            # Request is authenticated user trying to write,
            # we need to ensure new data relates to users existing organization
            return (
                request
                .user
                .organization
                .vendor_set
                .filter(id=request.data["vendor"])
                .exists()
            )

        return False

    def has_object_permission(self, request, view, obj):
        if (
            not request.user.is_anonymous and
            request.method not in SAFE_METHODS
        ):
            # Request is authenticated user trying to write,
            # we need to ensure new data relates to users existing organization
            return (
                request
                .user
                .organization
                .vendor_set
                .filter(id=obj.vendor.id)
                .exists()
            )

        return False


class CreateOnly(DefaultClosedBasePermission):
    def has_permission(self, request, view):
        return request.method == "POST"


class ReadWithAccessCode(DefaultClosedBasePermission):
    def has_object_permission(self, request, view, obj):
        return request.data["access_code"] == obj.access_code


class AllowNone(DefaultClosedBasePermission):
    def has_permission(self, request, view):
        return False

    def has_object_permission(self, request, view, obj):
        return False


class AllowOrganizationAdmin(DefaultClosedBasePermission):
    def has_permission(self, request, view):
        return (
            not request.user.is_anonymous and
            request
            .user
            .organization
            .vendor_set
            .filter(id=request.query_params["vendor"])
            .exists()
        )


class AllowWithAccessCode(DefaultClosedBasePermission):
    def has_permission(self, request, view):
        return False

    def has_object_permission(self, request, view, obj):
        return request.query_params["acces_code"] == obj.access_code
